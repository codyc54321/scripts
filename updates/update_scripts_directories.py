#!/usr/bin/env python

import os, sys

from ez_scrip_lib import *

SCRIPTS_PATH            = os.path.join(HOMEPATH, "scripts")
BASH_PROF_PATH          = os.path.join(HOMEPATH, '.bash_profile')
bash_prof_lines         = read_lines(BASH_PROF_PATH)
bash_prof_content       = read_content(BASH_PROF_PATH)

def get_category_contents(category):
    try:
        category_contents_rgx   = r"(?<={}[*]\n).*?(?=[#] END)".format(category)
        match       = re.search(category_contents_rgx, bash_prof_content, flags=re.DOTALL)
        contents    = match.group()
        return contents
    except:
        return None
        
def get_scripts_subfolders():
    for root, dirs, files in os.walk(SCRIPTS_PATH):
        scripts_dirs = []
        for d in dirs:
            if not d.startswith('.') and not d in ['old', 'in_progress', 'ruby_practice',
                                                   'apps_repo', 'atom_files', 'setup_scripts',
                                                   'ez_scrip_lib']:
                scripts_dirs.append(d)
        scripts_dirs.sort()
        return scripts_dirs

def get_scripts_subfolder_path(dirname):
    if dirname.endswith('scripts'):
        return dirname.upper() + "_PATH" + '="$SCRIPTS'  + "/" + dirname + '"'
    else:
        return dirname.upper() + "_SCRIPTS_PATH" + '="$SCRIPTS' + "/" + dirname + '"'

def get_current_subfolder_paths_content():
    subfolders = get_scripts_subfolders()
    paths = []
    for folder in subfolders:
        paths.append(get_scripts_subfolder_path(folder))
    return '\n'.join(paths)

directories_contents = get_category_contents('DIRECTORIES')

new_paths_content = get_current_subfolder_paths_content() + '\n'

replace_content(BASH_PROF_PATH, directories_contents, new_paths_content)