#!/usr/bin/env python

import subprocess, sys, time, os, re

from ez_scrip_lib import *
from ez_scrip_lib.my_git import update_project


DIRS        = ['projects', 'work_projects',]

PROJECTS    = ['scripts', 'my_documents',]


for this_dir in DIRS:
    projects = get_subdirectories(this_dir)
    for project in projects:
        path = get_project_path(project_name=project, relative_dir=this_dir)
        #TODO: if setup.py in project dir: python setup.py install
        update_project(path)

for project in PROJECTS:
    project_path = project
    path = os.path.join(HOMEPATH, project_path)
    # TODO: if setup.py in project dir: python setup.py install
    update_project(path)
