#!/usr/bin/env python

"""
just install in crontab:

    crontab -e

    * * * * * /usr/bin/python ~/path/update_files.py
"""

import sys, os, filecmp
from subprocess import Popen, PIPE
# from ez_scrip_lib.updates import pull_system_file_from_scripts_project_and_update_it, push_system_file_to_scripts_project

HOMEPATH          = os.path.expanduser('~')
SCRIPTS_PATH      = os.path.join(HOMEPATH, "scripts")

files = [
         {'real_path': '.bash_profile', 'repo_path': 'bash/bash_profile.sh'},
         {'real_path': '.bashrc', 'repo_path': 'bash/bashrc.sh'},
         {'real_path': '.bash_prompt', 'repo_path': 'bash/bash_prompt.sh'},
         {'real_path': '.atom/snippets.cson', 'repo_path': 'atom_files/snippets.cson'},
         {'real_path': '.atom/config.cson', 'repo_path': 'atom_files/config.cson'}
]


def append_content(the_file, content):
    try:
        current_content = read_content(the_file)
        if not content in current_content:
            with open(the_file, 'a') as f:
                f.write(content)
    except IOError:
        write_content(the_file, content)

def call_sp(command, **arg_list):
    p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, **arg_list)
    output, err = p.communicate()
    return output, err

def read_content(the_file):
    with open(the_file, 'r') as f:
        return f.read()

def write_content(the_file, content):
    with open(the_file, 'w') as f:
        f.write(content)


def get_full_repopath_and_realpath(repo_path=None, real_path=None):
    """repo_path is relative to ~/scripts; real_path is the path relative to HOME"""
    full_repo_path    = os.path.join(SCRIPTS_PATH, repo_path)
    full_real_path    = os.path.join(HOMEPATH, real_path)
    return full_repo_path, full_real_path


def initialize_file_in_scripts_project(source_path, relative_repo_path, repo_path):
    # make a scripts repo copy of the file and push if need be
    source_path = ensure_home(source_path)
    repo_path = ensure_home(repo_path)
    # initialize d down here so repo_path can be set as a full path by ensure_home()
    d = {'cwd': repo_path}
    full_repo_path = os.path.join(repo_path, relative_repo_path)
    write_content(full_repo_path, read_content(source_path))
    # uses relative repo_path since it's adding to the current scripts repo given in 'd' dictionary
    add_command = 'git add {}'.format(relative_repo_path)
    call_sp(add_command, **d)
    commit_command = 'git commit {} -m "{} autoupdate, initial commit of this file"'.format(full_repo_path, source_path)
    call_sp(commit_command, **d)
    call_sp('git push', **d)


def pull_system_file_from_scripts_project_and_update_it(real_path=None, repo_path=None):
    """repo_path is relative to ~/scripts; real_path is the path relative to HOME"""
    FULL_REPO_PATH, FULL_REAL_PATH = get_full_repopath_and_realpath(repo_path, real_path)
    d = {'cwd': SCRIPTS_PATH}
    if os.path.exists(FULL_REPO_PATH):
        output, err = call_sp('git pull', **d)
        print(err)
        content_after_pull = read_content(FULL_REPO_PATH)
        write_content(FULL_REAL_PATH, content_after_pull)
    else:
        initialize_file_in_scripts_project(FULL_REAL_PATH, repo_path, SCRIPTS_PATH)


def push_system_file_to_scripts_project(real_path=None, repo_path=None):
    FULL_REPO_PATH, FULL_REAL_PATH = get_full_repopath_and_realpath(repo_path, real_path)

    d = {'cwd': SCRIPTS_PATH}
    if os.path.exists(FULL_REPO_PATH):
        # filecmp.cmp returns True if 2 files are the same
        if not filecmp.cmp(FULL_REAL_PATH, FULL_REPO_PATH):
            new_written_content = read_content(FULL_REAL_PATH)
            write_content(FULL_REPO_PATH, new_written_content)
            call_sp('git commit {} -m "{} autoupdate"'.format(FULL_REPO_PATH, FULL_REAL_PATH), **d)
            _, err = call_sp('git push', **d)
            print(err)
            append_content('/tmp/cronlog.txt', err)
    else:
        initialize_file_in_scripts_project(FULL_REPO_PATH, FULL_REAL_PATH)

callbacks = {'pull': pull_system_file_from_scripts_project_and_update_it,
             'push': push_system_file_to_scripts_project}

args = sys.argv
purpose_arg = args[1]


for f in files:
    try:
        callbacks[purpose_arg](**f)
    except OSError:
        pass
