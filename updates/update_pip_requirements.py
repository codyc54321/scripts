#!/usr/bin/env python
# coding: utf-8

"""updates requirements.txt in the current directory you call script"""

import os, sys, subprocess, time, re

CWD = os.getcwd()

def get_output(command):
    return subprocess.check_output(command, shell=True)

def write_content(the_file, content):
    with open(the_file, 'w') as f:
        f.write(content)
        
args = sys.argv
if len(args) > 1:
    if args[1] == 'python3':
        python3 = True
    else:
        raise Exception('Unrecognized argument')
else:
    python3 = False
        
if python3:
    frz_command = 'pip3 freeze'
else:
    frz_command = 'pip freeze'
    
output = get_output(frz_command)

output_lines = output.split('\n')

clean_lines = []
for line in output_lines:
    if not line.startswith('-') and not line.startswith('dfa') and not line.startswith('webapi'):
        clean_lines.append(line)

clean_content = '\n'.join(clean_lines)

write_content(os.path.join(CWD, 'requirements.txt'), clean_content)