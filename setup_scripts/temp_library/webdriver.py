from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


def access_link_by_text(text, driver):
    try:
        block = driver.find_element_by_xpath("//a[normalize-space(text())='{text}']".format(text=text))
        link  = block.get_attribute('href')
        driver.get(link)
    except:
        block = driver.find_element_by_xpath("//a[child::span[normalize-space(text())='{text}']]".format(text=text))
        link  = block.get_attribute('href')
        driver.get(link)

def submit_form(button_text=None, html_id=None, html_name=None, driver=None):
    if button_text:
        form = driver.find_element_by_xpath("//input[@value='{button_text}']".format(button_text=button_text))
        form.submit()
    elif html_name:
        form = driver.find_element_by_name(html_name)
        form.submit()
    elif html_id:
        form = driver.find_element_by_id(html_id)
        form.submit()

def click_button(button_text=None, html_id=None, html_name=None, driver=None):
    if button_text:
        button = driver.find_element_by_xpath("//input[@value='{button_text}']".format(button_text=button_text))
        button.click()
    elif html_name:
        button = driver.find_element_by_name(html_name)
        button.click()
    elif html_id:
        button = driver.find_element_by_id(html_id)
        button.click()

def find_box_and_fill(html_id=None, html_name=None, value=None, driver=None):
    if html_id:
        box = driver.find_element_by_id(html_id)
        box.send_keys(value)
    elif html_name:
        box = driver.find_element_by_name(html_name)
        box.send_keys(value)

def pick_select_box(html_id=None, html_name=None, value=None, driver=None):
    if html_id:
        Select(driver.find_element_by_id(html_id)).select_by_value(value)
    elif html_name:
        Select(driver.find_element_by_name(html_name)).select_by_value(value)
    
def wait(an_expected_id, driver, delay=7):
    try:
        WebDriverWait(driver, delay).until(EC.presence_of_element_located(driver.find_element_by_id(an_expected_id)))
    except TimeoutException:
        print("Loading took too much time!")