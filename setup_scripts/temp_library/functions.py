import os, sys, subprocess, datetime, re, shutil

from .functions import MAIN_PASSWORD

HOMEPATH            = os.path.expanduser('~')
WORK_PROJECTS_PATH  = os.path.join(HOMEPATH, 'work_projects')


def run(command):
    subprocess.call(command, shell=True)

def call_sp(command, *args, **kwargs):
    sp = subprocess.Popen(command, shell=True, *args, **kwargs)
    sp.communicate()

def read_content(the_file):
    with file(the_file, 'r') as f:
        return f.read()

def read_lines(the_file):
    with open(the_file, 'r') as f:
        return f.readlines()

def write_content(the_file, content):
    with file(the_file, 'w') as f:
        f.write(content)

def append_content(the_file, content):
    with file(the_file, 'a') as f:
        f.write(content)

def append_if_not_in(destination_path, content):
    try:
        dest_content   = read_content(destination_path)
        if not content in dest_content:
            append_content(destination_path, content)
    except IOError:
        write_content(destination_path, content)

def get_todays_date():
    return datetime.date.today().strftime("%B %d, %Y")

def get_fullpath(path):
    if path.startswith(HOMEPATH):
        return path
    else:
        return os.path.join(HOMEPATH, path)

def ensure_dir_exists(path):
    if not path.startswith(HOMEPATH):
        path = os.path.join(HOMEPATH, path)
    try: os.makedirs(path)
    except: pass

def copy_only_if_needed(source, destination):
    if not os.path.exists(destination):
        shutil.copyfile(source, destination)


# installation and initial setup

def prime_terminal_with_password(password=MAIN_PASSWORD):
    run('echo {} | sudo -S ls'.format(password))

def install(program):
    run('echo {} | sudo -S apt-get install -y {}'.format(MAIN_PASSWORD, program))

def pip_install(package):
    run('sudo pip install {}'.format(package))

def update():
    run('sudo apt-get update')

def add_repo(repo):
    run('echo | sudo add-apt-repository {}'.format(repo))

def install_atom():
    add_repo('ppa:webupd8team/atom')
    update()
    install('atom')

def generate_keys():
    if not os.path.exists('~/.ssh/id_rsa.pub'):
        run('ssh-keygen')


# work specific

def get_work_project_path(project_name):
    if project_name == 'myproject':
        return os.path.join(WORK_PROJECTS_PATH, 'my/path')
    else:
        return os.path.join(WORK_PROJECTS_PATH, project_name)

def clone_work_project(project_name):
    path = get_work_project_path(project_name)
    ensure_dir_exists(path)
    d = {'cwd': path}
    call_sp('git clone ssh://git@theurl/{}.git {}'.format(project_name, path))


def replace_setting(setting_name=None, setting_rgx=None, new_setting=None, delimiter=":", project_name=None, relative_path=None, fullpath=None):
    """remember to include quotes around new setting, as not all settings need to be a string"""
    if not fullpath:
        filepath = os.path.join(get_work_project_path(project_name), relative_path)
    else:
        filepath = fullpath
    content = read_content(filepath)
    lines   = read_lines(filepath)
    rgxsub = setting_rgx or setting_name
    # print(rgxsub)
    rgx     = r"{}\s*[=]".format(rgxsub)
    for line in lines:
        match = re.search(rgx, line)
        if match:
            # print match.group()
            print(setting_name)
            new_setting_line    = setting_name + " = " + new_setting + '\n'
            print(new_setting_line)
            content             = content.replace(line, new_setting_line)
    write_content(filepath, content)

def check_if_git_index_clean():
    try:
        x = subprocess.check_output(['git', 'diff', '--quiet'])
        return True
    except:
        return False


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def gather(msg=None, default=None):
    while True:
        text = msg + '\n'
        if default:
            text += "the default is {default}...press enter to accept\n".format(default=default)
            answer = input(text)
            return answer or default
        answer = input(text)
        if not answer:
            continue
        return answer
