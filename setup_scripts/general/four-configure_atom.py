#!/usr/bin/env python

import os, shutil, sys, subprocess, time

HOMEPATH = os.path.expanduser('~')

def call_sp(command, **arg_list):
    p = subprocess.Popen(command, shell=True, **arg_list)
    p.communicate()

def get_parent_dir():
    myname          = os.path.basename(sys.argv[0])
    current_path    = os.path.realpath(__file__)
    return current_path.replace(myname, '')

def write_atom_file(filename):
    source = "~/scripts/atom_files/{filename}.cson".format(filename=filename)
    destination = "~/.atom/{filename}.cson".format(filename=filename)
    shutil.copyfile(source, destination)
    time.sleep(1)

ATOM_FILES = ['config', 'snippets']

for f in ATOM_FILES:
    write_atom_file(f)
    
