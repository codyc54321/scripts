#!/usr/bin/env python

import time, pyperclip, os, sys

MAIN_SETUP_DIR = os.path.realpath(__file__).replace(('/general/' + os.path.basename(sys.argv[0])), '')
sys.path.append(MAIN_SETUP_DIR)
from temp_library.functions import *
from temp_library.webdriver import *
from selenium.webdriver.support.ui import WebDriverWait


def setup_bitbucket_account():
    driver = webdriver.Firefox()

    driver.get("https://bitbucket.org")
    time.sleep(1)

    access_link_by_text("Log in", driver)

    find_box_and_fill(html_name="username", value="cchilder@mail.usf.edu",  driver=driver)
    # TODO: add passwords to a config somewhere
    find_box_and_fill(html_name="password", value=APE_NINE, driver=driver)
    submit_form(button_text="Log in", driver=driver)
    # wait('page', driver)
    time.sleep(6)
    access_link_by_text("Bitbucket settings", driver)
    access_link_by_text("SSH keys", driver)

    click_button(html_id="add-key", driver=driver)
    machine_name = "New machine " + get_todays_date()


the_key = read_content('{}/.ssh/id_rsa.pub'.format(HOMEPATH))
pyperclip.copy(the_key)
setup_bitbucket_account()


# https://confluence.atlassian.com/bitbucket/use-the-bitbucket-cloud-rest-apis-222724129.html

# https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html
