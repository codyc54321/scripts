#!/usr/bin/env python

import os, sys, subprocess, time, re

HOMEPATH = os.path.expanduser('~')

MAIN_SETUP_DIR = os.path.realpath(__file__).replace(('/general/' + os.path.basename(sys.argv[0])), '')
sys.path.append(MAIN_SETUP_DIR)
from temp_library.functions import *

prime_terminal_with_password()

def get_parent_dir():
    myname          = os.path.basename(sys.argv[0])
    current_path    = os.path.realpath(__file__)
    return current_path.replace(myname, '')

def install_from_shellscript(program):
    call_sp('sudo {}bash_install/install_{}.sh'.format(get_parent_dir(), program))

"""make sure to add atom repo before installations"""

INSTALL_LIST        = ['xdotool', 'python-pip', 'git', 'xclip', 'python-dev', 'python3-dev', 'python', 'apt-file',
                       'GDebi', 'python-tk', 'python3-tk', 'libpq-dev', 'gitg', 'python-xlib', 'sshpass',
                       'libsasl2-dev', 'python-dev', 'libldap2-dev', 'libssl-dev', 'libappindicator1']
LXML_STUFF          = ['libxml2-dev', 'libxslt1-dev', 'xserver-xephyr', 'xvfb']
RPM_INSTALL_PCKGS   = ['alien', 'dpkg-dev', 'debhelper', 'build-essential']
INSTALL_LIST        += LXML_STUFF + RPM_INSTALL_PCKGS

PIP_INSTALL_LIST    = ['selenium', 'requests', 'pyperclip', 'virtualenv', 'python-xlib', 'virtualenvwrapper', 'ipython', 'ipdb', 'pexpect', 'lxml']

BASH_INSTALL_LIST   = ['chrome', 'pycharm'] #, 'android_studio']

"""start work"""
generate_keys()

install_atom()

update()

install(' '.join(INSTALL_LIST))

call_sp('sudo apt-get -f install') # ensure new dependencies get added too

pip_install(' '.join(PIP_INSTALL_LIST))

for program in BASH_INSTALL_LIST:
    install_from_shellscript(program)
    
