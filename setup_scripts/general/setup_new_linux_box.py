#!/usr/bin/env python

"""pass me 1 for the first stage, and 2 for the next:

   ./setup_new_linux_box 1              """

import os, sys, subprocess, time

HOMEPATH = os.path.expanduser('~')

MAIN_SETUP_DIR = os.path.realpath(__file__).replace(('/general/' + os.path.basename(sys.argv[0])), '')
sys.path.append(MAIN_SETUP_DIR)
from temp_library.functions import *

def get_parent_dir():
    myname          = os.path.basename(sys.argv[0])
    current_path    = os.path.realpath(__file__)
    return current_path.replace(myname, '')


#if sys.argv[1] == '1':
#   SCRIPTS = ['one-install_all.py', 'two-setup_bitbucket.py']
#elif sys.argv[1] == '2':
#   SCRIPTS = ['three-clone_projects_configure_git_setup_cronjobs.py',]


SCRIPTS = ['one-install_all.py',  'three-clone_projects_configure_git_setup_cronjobs.py', 'four-configure_atom.py']

for script in SCRIPTS:
    parent_dir = get_parent_dir()
    path = os.path.join(parent_dir, script)
    run('python {}'.format(path))


#run('python ~/scripts/git_and_bitbucket/clone_all.py')
