#!/usr/bin/env python

import os, sys, subprocess, time

HOMEPATH = os.path.expanduser('~')
BASHRC_PATH = os.path.join(HOMEPATH, '.bashrc')

MAIN_SETUP_DIR = os.path.realpath(__file__).replace(('/general/' + os.path.basename(sys.argv[0])), '')
sys.path.append(MAIN_SETUP_DIR)
from temp_library.functions import *
from temp_library.settings import *


def get_parent_dir():
    myname          = os.path.basename(sys.argv[0])
    current_path    = os.path.realpath(__file__)
    return current_path.replace(myname, '')


# source, destination
# WRITE_PATHS = [('scripts/bash/bashrc.sh',          '.bashrc'),
#                ('scripts/bash/bash_profile.sh',    '.bash_profile'),
#                ('scripts/bash/bash_prompt.sh',     '.bash_prompt')]
# 
# for tup in WRITE_PATHS:
#     read_path  = os.path.join(HOMEPATH, tup[0])
#     write_path = os.path.join(HOMEPATH, tup[1])
#     content_to_append = read_content(read_path)
#     append_if_not_in(write_path, content_to_append)


"""sourcing isn't working from this script"""
# source_command = '. {}'.format(BASHRC_PATH)
# call_sp(source_command)

# basic git setup
GIT_CONFIG_TEXT = """\
[user]
        email = cody.childers@dimensional.com
        name = Cody Childers

[alias]
        b           = branch
        p           = push
        co          = checkout
        cm          = commit -m
        cmall       = !git add -A && git commit -m
        cob         = checkout -b
        cached      = diff --cached
        addall      = add -A
        editconfig  = config --global -e
        save        = !git add -A && git commit -m SAVEPOINT
        unsave      = reset HEAD~1 --mixed
        amend       = commit -a --amend
        wipe        = !git add -A && git commit -qm 'WIPE SAVEPOINT' && git reset HEAD~1 --hard
[push]
        default = matching"""

GIT_CONFIG_CREATE_COMMAND = 'echo Summer3153 | sudo -S echo "{}" > {}/.gitconfig'.format( GIT_CONFIG_TEXT, HOMEPATH)

run(GIT_CONFIG_CREATE_COMMAND)

#cronjobs
CRONTAB_PATH = os.path.join(get_parent_dir(), 'crontab.txt')

# call_sp('crontab {}'.format(CRONTAB_PATH))

print("Did the cronjobs work? Printing 'crontab -l'")
subprocess.call(['crontab', '-l'])
