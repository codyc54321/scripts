#!/usr/bin/env python

import os, sys, subprocess, time, re

"""
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
"""

HOMEPATH = os.path.expanduser('~')

relative_path_to_remove = os.path.join('/general/bash_install/' + os.path.basename(sys.argv[0]))

MAIN_SETUP_DIR = os.path.realpath(__file__).replace(relative_path_to_remove, '')
sys.path.append(MAIN_SETUP_DIR)
from temp_library.functions import *

prime_terminal_with_password()

update()
call_sp('sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927')
call_sp('echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list')
update()
install('mongodb-org')

MONGO_SERVICE_FILE = """\
[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target
Documentation=https://docs.mongodb.org/manual

[Service]
User=mongodb
Group=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

[Install]
WantedBy=multi-user.target
"""

def generate_service_file():
    MONGO_SERVICE_PATH = "/etc/systemd/system/mongodb.service"
    # MONGO_SERVICE_PATH = "/lib/systemd/system/mongod.service"
    if not os.path.exists(MONGO_SERVICE_PATH):
        call_sp('sudo touch {}'.format(MONGO_SERVICE_PATH))
        call_sp('sudo echo "{}" | sudo tee {}'.format(MONGO_SERVICE_FILE, MONGO_SERVICE_PATH))


call_sp('sudo service mongod start')
call_sp('sudo service mongod status')

# LOG_FILE = "/var/log/mongodb/mongod.log"
# 
# log_content = read_content(LOG_FILE)
# 
# EXPECTED_CONTENT = "waiting for connections on port"
# 
# if EXPECTED_CONTENT in log_content:
#     print('\n\nMongo is up and running!!!')
# else:
#     print("\n\nMongo didn't work...")

