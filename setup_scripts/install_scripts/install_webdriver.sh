sudo pip install -U selenium
GECKO_RELEASE="v0.13.0"
GECKO_FILE="geckodriver-$GECKO_RELEASE-linux64.tar.gz"
wget https://github.com/mozilla/geckodriver/releases/download/$GECKO_RELEASE/$GECKO_FILE
sudo tar -xzf $GECKO_FILE --directory /usr/local/bin
rm $GECKO_FILE
