
# Tested for Ubuntu 15.10

# YOU MUST RUN THIS WITH SUDO!!!
#
# sudo ./install_docker.sh

echo $THE_USUAL | sudo -S apt-get update
sudo apt-get install apt-transport-https ca-certificates -y
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo echo "deb https://apt.dockerproject.org/repo ubuntu-wily main" > /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get purge lxc-docker
sudo apt-get install linux-image-extra-$(uname -r) -y
sudo apt-get update
sudo apt-get install docker-engine -y
sudo service docker start
sudo docker run hello-world
sudo groupadd docker
sudo usermod -aG docker cchilders
sudo sed -i "s:ExecStart=/usr/bin/dockerd -H fd\://:ExecStart=/usr/bin/docker daemon --dns 10.96.69.8 -H fd\://:" /lib/systemd/system/docker.service
sudo systemctl daemon-reload && sudo systemctl restart docker.service
sudo pip install docker-compose
sudo docker-compose -f ~/work_projects/ci/webapi/docker-compose.yml run -p 99:99 webapi_develop
