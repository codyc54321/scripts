# Firefox 36 works with Python Selenium webdriver...
wget ftp.mozilla.org/pub/mozilla.org/firefox/releases/36.0/linux-x86_64/en-US/firefox-36.0.tar.bz2
tar -xjvf firefox-36.0.tar.bz2
sudo rm -rf /opt/firefox*
sudo mv firefox /opt/firefox36.0
sudo ln -sf /opt/firefox36.0/firefox /usr/bin/firefox
rm firefox-36.0.tar.bz2