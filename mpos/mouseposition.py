#!/usr/bin/env python3

# mousepos.py (linux only)
"""module mousepos"""

# uses the package python-xlib
# from http://snipplr.com/view/19188/mouseposition-on-linux-via-xlib/

import os, sys, time
from Xlib import display
# Tried on Mac: x11 didn't work...
# import tkinter
# tkint = tkinter.Tk()

HOMEPATH = os.path.expanduser('~')
SILO_PATH = HOMEPATH + "/scripts/mpos/mousepositionsilo.sh"


"""
sends commands to scripts/tmp/mousepositionsilo.txt like:

xdotool mousemove 1341 477
sleep 0.1
xdotool click 1

give me the option 'restart' to start a fresh file
"""


COMMAND_PROMPT = "\
Hit enter for a left click, 'r' for a right click,\n\
'd' for a double click, 'h' to hover the mouse, or 'q' to quit:\n"


def read_content(the_file):
    with open(the_file, 'r') as f:
        return f.read()

def append_content(the_file, content):
    try:
        current_content = read_content(the_file)
        if not content in current_content:
            with open(the_file, 'a') as f:
                f.write(content)
    except IOError:
        write_content(the_file, content)

def clear_content(the_file):
    with open(the_file, 'w') as f:
        f.write('')

def get_mousepos():
    """mousepos() --> (x, y) get the mouse coordinates on the screen (linux, Xlib)."""
    # Tried on Mac: x11 didn't work...
    # x, y = tkint.winfo_pointerxy()
    # return x, y
    data = display.Display().screen().root.query_pointer()._data
    return data["root_x"], data["root_y"]

def get_and_write_new_command(start_time=None):
    answer = input(COMMAND_PROMPT)
    if answer == 'q':
        sys.exit()
    elif answer == '':
        answer = 'l'
    if start_time:
        end_time = time.time()
        difference = end_time - start_time
        sleep_command = "sleep {}".format(difference)
    else:
        sleep_command = ""
    command = answer.lower()
    mapper = {'l': '1', 'r': '3', 'd': '1 --repeat 2'}
    x, y = get_mousepos()
    if answer == 'h':
        commands_string = """{sleep_command}\nxdotool mousemove {x} {y}\nsleep 0.3\n\n""" \
                   .format(x=x, y=y, sleep_command=sleep_command)
    else:
        commands_string = """{sleep_command}\nxdotool mousemove {x} {y}\nsleep 0.3\nxdotool click {command}\n\n""" \
                   .format(x=x, y=y, sleep_command=sleep_command, command=mapper[command])
    append_content(SILO_PATH, commands_string)
    return commands_string


if __name__ == "__main__":

    try:
        if sys.argv[1] == 'restart':
            clear_content(SILO_PATH)
            get_and_write_new_command()
            while True:
                start_time = time.time()
                get_and_write_new_command(start_time=start_time)
        elif sys.argv[1] == 'blank':
            commands = get_and_write_new_command()
            print("\n\n" + commands)
    except IndexError:
        commands = get_and_write_new_command()
        append_content(SILO_PATH, commands)
        print("\n\n" + commands)
