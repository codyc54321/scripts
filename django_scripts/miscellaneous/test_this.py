#!/usr/bin/env python
# coding: utf-8

"""
calls django testing system from your current dir

call alone to run all tests in your current dir:
    
    testthis
    user:~/work_projects/webapi/webapi/tests/test_client [ckc/webapi_test_coverage]$ testthis 
    Creating test database for alias 'default'... 
    
call with filenames as args to run specific files:

    user:~/work_projects/webapi/webapi/tests/test_client [ckc/webapi_test_coverage]$ testthis test_windows_services.py test_regions.py
    Creating test database for alias 'default'...

this runs both files as tests. it can take the filename with .py or without it
"""


import os, sys, subprocess, time


CWD = os.getcwd()

def call_sp(command, **arg_list):
    p = subprocess.Popen(command, shell=True, **arg_list)
    p.communicate()
    

def find_manage_dot_py(current):
    for root, _, files in os.walk(current):
        for name in files:
            if name == "manage.py":
                return os.path.join(root, "manage.py")
        
    if os.path.dirname(current) == current:
        raise Exception("Reached root and couldn't find manage.py...are you sure this a django project?")
    next_attempt = os.path.dirname(current)
    return find_manage_dot_py(next_attempt)


def run_tests(files, management_path):
    root_path = os.path.dirname(management_path)
    relative_path_w_left_slash = CWD.replace(root_path, "")
    relative_path_w_slashes = relative_path_w_left_slash[1:]
    relative_path = relative_path_w_slashes.replace("/", ".")
    if files:
        for f in files:
            filename = f.replace(".py", "")
            test_path = relative_path + "." + filename
            test_command = "python " + management_path + " test " + test_path
            call_sp(test_command)
    else:
        test_command = "python " + management_path + " test " + relative_path
        call_sp(test_command)
        
        
files_to_test = sys.argv[1:]

run_tests(files_to_test, find_manage_dot_py(CWD))
