#!/usr/bin/env python
import re

import django
django_path_init = django.__file__
rgx = "(?P<not_init>.*)/(?P<init>__init__\..*)"
match = re.search(rgx, django_path_init)
django_path = match.group('not_init')
print(django_path)


#there is a script to grab all classes for a file in the unit_test project at work. push that, and update it to grab function names too