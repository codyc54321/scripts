#!/usr/bin/env python

import sys

from ez_scrip_lib.django_scripting  import register_app_in_settings_file
from ez_scrip_lib.sysadmin          import CWD

apps_to_install = sys.argv[1:]

register_app_in_settings_file(project_path=CWD, apps_to_install=apps_to_install)
