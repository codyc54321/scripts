#!/usr/bin/env python

"""setup basic project, configured with templates dir and whatnot"""

from ez_scrip_lib import *

ACCOUNT_APP_PATH = "~/scripts/apps_repo/accounts"
BASE_TEMPLATE_PATH = "~/scripts/apps_repo/base.html"
#-------------------------------------------------------------------------------------------------------------------------

project_name = sys.argv[1]
project_path = os.path.join(CWD, project_name)

call_sp('django-admin startproject {}'.format(project_name), **{'cwd': CWD})
time.sleep(2)

settings_path = get_settings_path(project_path)
settings_content = read_content(settings_path)

template_dir = os.path.join(project_path, 'templates')
ensure_dir_exists(template_dir)

replace_content(settings_path, "'DIRS': [],", "'DIRS': [os.path.join(BASE_DIR, 'templates')],")

call_sp("cp -r {account_app} {project_path}".format(account_app=ACCOUNT_APP_PATH, project_path=project_path))
call_sp("cp -r {base_template} {template_dir}".format(base_template=BASE_TEMPLATE_PATH, template_dir=template_dir))

apps_to_install = ['accounts', 'django_extensions', 'bootstrapform']
register_app_in_settings_file(settings_path=settings_path, apps_to_install=apps_to_install)

call_sp("""source /usr/local/bin/virtualenvwrapper.sh && mkvirtualenv --no-site-packages {}"""\
        .format(project_name), **{'executable': 'bash'})

# install several commonly used apps, like django extenstions, bootstrapforms, etc
PIP_INSTALL_LIST = ['django', 'django-bootstrap-form', 'django_extensions', 'ipython', 'ipdb']

PIP_INSTALL_COMMAND = "~/.virtualenvs/{project}/bin/pip3 install {package}"

for package in PIP_INSTALL_LIST:
    call_sp(PIP_INSTALL_COMMAND.format(project=project_name, package=package), **{'cwd': project_path})

call_sp('pipupdate', **{'cwd': project_path})
