#!/bin/bash

# http://www.manniwood.com/postgresql_and_bash_stuff/
# https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04
# https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-django-with-postgres-nginx-and-gunicorn

# http://stackoverflow.com/questions/1988249/how-do-i-use-su-to-execute-the-rest-of-the-bash-script-as-that-user

set -e
set -u

apt-get update
apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib

# su - postgres
# createdb nyble
# createuser -P


RUN_ON_MYDB="psql "


su -c "cd /home/$USERNAME/$PROJECT ; svn update" -m "$USERNAME" 

RUN_ON_MYDB="psql -X -U postgres --set ON_ERROR_STOP=on --set AUTOCOMMIT=off mydb"

$RUN_ON_MYDB <<SQL
CREATE DATABASE nyble;
CREATE USER cchilders WITH PASSWORD '$MYPW';
GRANT ALL PRIVILEGES ON DATABASE mydb TO myuser;
SQL
