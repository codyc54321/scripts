from flask import current_app
from flask.ext.script import Command, Option
from flask.ext.security.recoverable import update_password
from werkzeug.local import LocalProxy
from alembic.config import command
from alembic.config import Config as AlembicConfig
import codecs
from collections import Counter
import csv
import os
from author.model.utils import now
from migrations.ddl_manager import install
from tempfile import TemporaryDirectory
from getpass import getpass


_datastore = LocalProxy(lambda: current_app.extensions['security'].datastore)


def commit(fn):
    def wrapper(*args, **kwargs):
        fn(*args, **kwargs)
        _datastore.commit()
    return wrapper


class ConfirmUserCommand(Command):
    option_list = (
        Option('-u', '--user', dest='user_identifier'),
    )

    @commit
    def run(self, user_identifier):
        user = _datastore.get_user(user_identifier)
        if user:
            user.confirmed_at = now()
            print("User '%s' has been confirmed" % user_identifier)
        else:
            print("Could not find user with identifier: %s" % user_identifier)


class ChangeUserPassword(Command):

    option_list = (
        Option('-u', '--user', required=True, dest='user_identifier'),
    )

    @commit
    def run(self, user_identifier):
        user = _datastore.get_user(user_identifier)
        if user:
            print("Enter new password for user: %s" % (user_identifier,))
            password = getpass("Password: ")
            password_confirm = getpass("Confirm Password: ")
            if password != password_confirm:
                print("Passwords do not match")
            else:
                update_password(user, password)
                print("User '%s' password has been changed" % user_identifier)
        else:
            print("Could not find user with identifier: %s" % user_identifier)


class DeleteSubmission(Command):
    option_list = (
        Option('-s', '--submission', dest='submission_id'),
        Option('-d', '--debug', action='store_true', default=False, dest='debug')
    )

    @commit
    def run(self, submission_id, debug):
        ds = DeleteSubmision()
        ds.run(submission_id, debug)


class DeleteOrganization(Command):
    option_list = (
        Option('-o', '--organization', dest='organization_id'),
        Option('-d', '--debug', action='store_true', default=False, dest='debug')
    )

    @commit
    def run(self, organization_id, debug):
        delete_org = DeleteOrganizationRunner()
        delete_org.run(organization_id, debug)


class CloseAccount(Command):
    option_list = (
        Option('-u', '--user', required=True, dest='user_identifier'),
        Option('-d', '--d', action='store_true', default=False, dest='debug'),
    )

    @commit
    def run(self, user_identifier, debug):
        pw = PurgeWriter()
        if user_identifier == 'all':
            pw.run_all(debug)
        else:
            pw.run_one(user_identifier, debug)


class CancelSubscription(Command):
    option_list = (
        Option('-u', '--user', dest='user_identifier'),
        Option('-kd', '--keep_discovery', action='store_true', default=False, dest='keep_discovery'),
        Option('-d', '--debug', action='store_true', default=False, dest='debug'),
    )

    @commit
    def run(self, user_identifier, keep_discovery, debug):
        pw = PurgeWriter()
        pw.run_cancel_subscription(user_identifier, keep_discovery, debug)


class RecreateDatabase(Command):

    @commit
    def run(self):
        env = current_app.config.get('APP_CONFIG')
        database_url = current_app.config.get('SQLALCHEMY_DATABASE_URI')
        local_hosts = ('localhost', '127.0.0.1', '0.0.0.0')
        local = [host for host in local_hosts if host in database_url]
        if env == 'prod' or env == 'stage' or not local:
            print("Recreating the database is not permitted!")
            print("Env: %s   Database: %s" % (env, database_url))
        else:
            db.recreate_db()
            install(db.engine.execute)
            db.populate_db('data/database.yml', 'data/users.yml', 'data/publisher.yml', 'data/writer.yml', 'data/contact.yml', 'data/submission.yml')
            alembic_cfg = AlembicConfig("migrations/alembic.ini")  # cmd_vars=vars(options)
            command.stamp(alembic_cfg, "head")
            print("Created and populated database successfully.")


class PopulateDatabase(Command):

    option_list = (
        Option('-f', '--file', dest='filename'),
    )

    @commit
    def run(self, filename):
        db.populate_db(filename)
        alembic_cfg = AlembicConfig("migrations/alembic.ini")  # cmd_vars=vars(options)
        command.stamp(alembic_cfg, "head")
        print("Created and populated database successfully.")


class CreateCoupon(Command):

    option_list = (
        Option('-f', '--file', dest='filename'),
    )

    @commit
    def run(self, filename):
        import chargebee
        import json
        coupon = json.load(open(filename))
        result = chargebee.Coupon.create(coupon)
        print(result.coupon)

class PurifySmartquotes(Command):

    @commit
    def run(self):
        pass

class AddToDiscovery(Command):

    option_list = (
        Option('-d', '--d', action='store_true', default=False, dest='debug'),
        Option('-e', '--send_email', action='store_true', default=False, dest='send_email'),
    )

    @commit
    def run(self, debug, send_email):
        print("Search for projects to add to discovery")
        projects = find_projects_to_add_to_discovery()
        if not debug:
            add_projects_to_discovery(projects, current_app, send_email)


class ExecuteSqlFile(Command):
    option_list = (
        Option('-f', '--file', dest='filename'),
        # Option('-nc', '--nocommit', action='store_true', default=False,  dest='nocommit')
    )

    def run(self, filename):
        self.run_should_be_nocommit(filename)
    # def run(self, filename, nocommit):
    #     print(nocommit)
    #     if nocommit:
    #         self.run_nocommit(filename)
    #     else:
    #         self.run_should_commit(filename)

    def run_should_be_nocommit(self, filename):
        # print("nocommit")
        connection = db.engine.connect()
        connection.connection.connection.set_isolation_level(0)
        execute(connection.execute, filename)
        connection.connection.connection.set_isolation_level(1)
        print("Executed sql file: %s" % (filename,))

    # def run_test_nocommit(self, filename):
    #     print("commit")
    #     connection = db.engine.connect()
    #     execute(connection.execute, filename)
    #     print("Executed sql file: %s" % (filename,))
    #
    # @commit
    # def run_should_commit(self, filename):
    #     execute(db.engine.execute, filename)
    #     print("Executed sql file: %s" % (filename,))


class ExecuteSql(Command):

    option_list = (
        Option('-c', '--connection', dest='connection', default='postgres://author:me@localhost:5432/author'),
        Option('-q', '--query', dest='query', help="select statement. Variables are expressed as :variable"),
        Option('-p', '--parameters', dest='parameters', default={}, help="JSON document with key-value pairs"),
    )

    # @commit
    def run(self, connection, query, parameters):
        # from alembic import op
        # from .sql import execute
        # print(op.execute(query))
        import json
        import sqlalchemy
        from sqlalchemy.orm import sessionmaker, scoped_session

        engine = sqlalchemy.create_engine(connection)
        Session = scoped_session(sessionmaker(bind=engine))

        if parameters:
            parameters = json.loads(parameters)

        s = Session()
        # result = s.execute('SELECT * FROM users WHERE id = :val', {'val': 1})
        result = s.execute(query, parameters)

        from collections import namedtuple

        Record = namedtuple('Record', result.keys())
        records = [Record(*r) for r in result.fetchall()]
        for r in records:
            print(r)


class S3Storage():
    def __init__(self, s3, filename):
        self.s3 = s3
        self.filename = filename
        self.source_bucket = current_app.config['AWS_UPLOAD_BUCKET_NAME']

    def delete(self):
        self.s3.delete_file(self.filename, self.source_bucket)

    def save(self, file_path):
        key = self.s3.get_bucket_key(self.source_bucket)
        self.s3.download_file(self.filename, file_path, key)


class UploadFile(Command):

    option_list = (
        Option('-f', '--file', dest='filename'),
        Option('-p', '--project', dest='project_id'),
        Option('-u', '--user', dest='email'),
        Option('-t', '--type', dest='file_type', default='manuscript')
    )

    def file_path(self, file_dir, file_type, item):
        return os.path.join(file_dir, file_type, item[file_type].strip())

    def upload(self, user, field_name, project, filename):
        organization = None
        contact = None
        writer = project.writer
        file = create_and_upload_file(
                None,
                filename,
                field_name,
                user,
                project,
                organization,
                writer,
                contact,
                self.storage)
        print(file)

    @commit
    def run(self, filename, project_id, email, file_type):
        # validate data
        self.storage = S3Storage(current_app.config['S3'], filename)
        user = get_user_by_email(email)
        if not user:
            raise("User %s does not exist" % (email,))

        project = Project.get_by_id(project_id)
        try:
            if project:
                if getattr(project, file_type):
                    print("%s document already uploaded for project id: %s" % (file_type, project_id,))
                else:
                    self.create_files(file_type, project, user, filename)
                    db.session.commit()
                    self.storage.delete()
                    print("deleted source: %s" % (filename,))
            else:
                print("Project not found. Id: '%s'" % (project_id,))
        except Exception:
            db.session.rollback()
            raise

    def create_files(self, file_type, project, user, filename):
        # verify files
        if filename:
            self.upload(user, file_type, project, filename)
            print("uploaded: %s" % (filename,))


class UpdateProjectML(Command):

    option_list = (
        Option('-f', '--file', dest='filename'),
    )

    @commit
    def run(self, filename):
        self.storage = S3Storage(current_app.config['S3'], filename)
        try:
            with TemporaryDirectory() as temp_path:
                local_filename = os.path.join(temp_path, filename)
                self.storage.save(local_filename)
                print("Updating project_ml from {}".format(filename))
                self.process_file(local_filename)
                db.session.commit()
                self.storage.delete()
                print("Deleted S3 file: %s" % (filename,))
        except Exception:
            db.session.rollback()
            raise

    def process_file(self, filename):
        project_ml_predictions = {}
        counter = Counter()
        # collect all the predictions
        with codecs.open(filename,'r', 'utf-8') as fin:
            dr = csv.DictReader(fin, delimiter=',')

            for record in dr:
                project_id = int(record['project_id'])
                accepted_prediction = float(record['accepted_prediction'])
                project_ml_predictions[project_id] = accepted_prediction

        # update existing records
        for project_ml in ProjectML.query.all():
            if project_ml.project_id in project_ml_predictions:
                db.session.add(project_ml)
                project_ml.accepted_prediction = project_ml_predictions.pop(project_ml.project_id)
                counter['updated'] += 1

        # insert the remaining records
        for project_id, accepted_prediction in project_ml_predictions.items():
            db.session.add(ProjectML(project_id=project_id, accepted_prediction=accepted_prediction))
            counter['inserted'] += 1

        print(counter.most_common())
