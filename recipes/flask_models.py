# -*- coding: utf-8 -*-

from sqlalchemy.orm import relationship
from sqlalchemy.orm.exc import NoResultFound
from myapp.model.validation import to_int
from myapp.extensions import db
from myapp.utils import purify_nonunicode_characters
from datetime import datetime

# Alias common SQLAlchemy names
Column = db.Column
relationship = relationship
DateTime = db.DateTime


class CRUDMixin(object):
    """Mixin that adds convenience methods for CRUD (create, read, update, delete)
    operations.
    """

    @classmethod
    def create(cls, **kwargs):
        """Create a new record and save it the database."""
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit=True, **kwargs):
        """Update specific fields of a record."""
        for attr, value in kwargs.iteritems():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        """Save the record."""
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        """Remove the record from the database."""
        db.session.delete(self)
        return commit and db.session.commit()


class Model(CRUDMixin, db.Model):
    """Base model class that includes CRUD convenience methods."""
    __abstract__ = True

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            kwargs[key] = purify_nonunicode_characters(value)
        db.Model.__init__(self, **kwargs)
    

# From Mike Bayer's "Building the app" talk
# https://speakerdeck.com/zzzeek/building-the-app
class SurrogatePK(object):
    """A mixin that adds a surrogate integer 'primary key' column named
    ``id`` to any declarative-mapped class.
    """
    __table_args__ = {'extend_existing': True}

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def get_by_id(cls, id):
        _id = to_int(id)
        if _id is not None:
            return cls.query.get(_id)
        return None


def ReferenceCol(tablename, nullable=False, pk_name='id', **kwargs):
    """Column that adds primary key foreign key reference.

    Usage: ::

        category_id = ReferenceCol('category')
        category = relationship('Category', backref='categories')
    """
    return db.Column(
        db.ForeignKey("{0}.{1}".format(tablename, pk_name)),
        nullable=nullable, **kwargs)


class CreatedTimestampMixin(object):
    #: Timestamp for when this instance was created, in UTC
    created_at = Column(DateTime, default=datetime.utcnow, nullable=False)


class TimestampMixin(CreatedTimestampMixin):
    """
    Provides the :attr:`created_at` and :attr:`updated_at` audit timestamps
    """
    #: Timestamp for when this instance was last updated (via the app), in UTC
    updated_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False)


class FullNameMixin(object):
    first_name = Column(db.String(60), nullable=True)
    last_name = Column(db.String(60), nullable=True)

    @property
    def full_name(self):
        return "{0} {1}".format(self.first_name, self.last_name)

    @property
    def short_name(self):
        last_name = ""
        if self.last_name and len(self.last_name):
            last_name = self.last_name[0] + "."
        return "{0} {1}".format(self.first_name, last_name)


class NameMixin(object):
    name = Column(db.String(200), nullable=False)
    active = Column(db.Boolean, nullable=False, default=True)

    def __repr__(self):
        return self.name


class CodeMixin(NameMixin):
    # TODO: Index naming schema?  index=True?
    code = Column(db.String(30), unique=True, nullable=True)

    @classmethod
    def get_by_code(cls, code):
        if code is not None:
            try:
                return cls.query.filter(cls.code==code).one()
            except NoResultFound:
                return None
        return None


class LocationMixin(object):
    # TODO: Find out what the Google Places API returns
    # TODO: Use pycountry
    # TODO: Include this Mixing when implementing Address
    city = Column(db.String(80), nullable=True)
    region = Column(db.String(80), nullable=True)  # state_prov
    country = Column(db.String(80), nullable=True)
    # TODO: Add geo data: http://geoalchemy.readthedocs.org/en/0.5/tutorial.html#setting-up-postgis
    # HEROKU:
    # https://devcenter.heroku.com/articles/heroku-postgres-extensions-postgis-full-text-search#full-text-search-dictionaries
    # https://devcenter.heroku.com/articles/postgis
    #
    def __repr__(self):
        return "{0}, {1}".format(self.city, self.region)


# def to_int(id):
#     if any(
#         (isinstance(id, basestring) and id.isdigit(),
#          isinstance(id, (int, float))),
#     ):
#         return int(id)
#     return None
