#!/bin/bash

#takes file_name and starts it in script folder

SCR_DIR="~/scripts"


if [ "$1" != '' ]; then
  echo '#!/usr/bin/env python' > "$SCR_DIR/$1.py"
  echo '# coding: utf-8' >> "$SCR_DIR/$1.py"
  echo '' >> "$SCR_DIR/$1.py"
  echo 'import os, sys, subprocess, time, re' >> "$SCR_DIR/$1.py"
  echo '' >> "$SCR_DIR/$1.py"
  echo 'from ez_scrip_lib import *' >> "$SCR_DIR/$1.py"
  echo '' >> "$SCR_DIR/$1.py"
  echo '' >> "$SCR_DIR/$1.py"
  chmod +x "$SCR_DIR/$1.py"
  atom "$SCR_DIR/$1.py"
else
  echo "Please enter a filename:"
  read NAME
  echo '#!/usr/bin/env python' > "$SCR_DIR/$NAME.py"
  echo '# coding: utf-8' >> "$SCR_DIR/$NAME.py"
  echo '' >> "$SCR_DIR/$NAME.py"
  echo '' >> "$SCR_DIR/$NAME.py"
  echo 'import os, sys, subprocess, time, re' >> "$SCR_DIR/$NAME.py"
  echo '' >> "$SCR_DIR/$NAME.py"
  echo 'from ez_scrip_lib import *' >> "$SCR_DIR/$NAME.py"
  echo '' >> "$SCR_DIR/$NAME.py"
  echo '' >> "$SCR_DIR/$1.py"
  chmod +x "$SCR_DIR/$NAME.py"
  atom "$SCR_DIR/$NAME.py"
fi
