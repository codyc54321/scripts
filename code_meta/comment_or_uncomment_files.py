#!/usr/bin/env python

import sys, os

from ez_scrip_lib import write_lines, read_lines, join, HOMEPATH, WORK_PROJECT_PATH, CWD

# resuable by editing 'paths' var
paths = []

def comment_out_file(filepath):
    lines = read_lines(filepath)
    new_lines = []
    for line in lines:
        new_lines.append('#' + line)
    write_lines(filepath, new_lines)
    
def uncomment_out_file(filepath):
    lines = read_lines(filepath)
    new_lines = []
    for line in lines:
        if line.startswith('#'):
            new_lines.append(line[1:])
        else:
            new_lines.append(line)
    write_lines(filepath, new_lines)


FUNC_MAPPER = {'c': comment_out_file, 'u': uncomment_out_file}

try:
    task = sys.argv[1]
except:
    raise Exception("Please give me an arg of 'c' to comment out, or 'u' to uncomment the files")
    
try:
    rel_starting_paths = sys.argv[2:]
    for path in rel_starting_paths:
        paths.append(os.path.realpath(path))
except:
    pass
    

for path in paths:
    for root, dirs, files in os.walk(path):
        for f in files:
            fullpath = join(root, f)
            FUNC_MAPPER[task](fullpath)
        dirs[:] = []
                


