#!/usr/bin/env python
# coding: utf-8


import os, sys, subprocess, time, re

from ez_scrip_lib import *

WEBAPI_ROOT = "~/work_projects/webapi"

URLS_PATH = join(WEBAPI_ROOT, 'webapi/server/db/dimensional/urls.py')

TESTFILE_PATH = join(WEBAPI_ROOT, 'webapi/tests_localdb/test_server/test_dimensional.py')

urls_lines = read_lines(URLS_PATH)

testfile_lines = read_lines(TESTFILE_PATH)




def get_url_names_mappings(lines):
    urls_rgx = r"""(?P<view>\w+)[.]as_view[(),]+\s*name[=]['](?P<url_name>[a-z-]+)[']"""
    mapper = {}
    for line in lines:
        match = re.search(urls_rgx, line)
        if match:
            mapper[match.group('view')] = match.group('url_name')
    del mapper['']
    return mapper
            
            
view_to_url_names_map = get_url_names_mappings(urls_lines)    
print(view_to_url_names_map)

            
def rewrite_testfile(testfile_lines, names_map):
    applicable_views = names_map.keys()
    print(applicable_views)
    
    new_lines = []
    for line in testfile_lines:
        new_lines.append(line)
        for view in applicable_views:
            if view in line:
                url_name_line = (" " * 4) + "url_name = 'dimensional:" + names_map[view] + "'\n"
                print(url_name_line)
                new_lines.append(url_name_line)
                break
    write_lines('~/rewrite_test.py', new_lines)
    
        
rewrite_testfile(testfile_lines, view_to_url_names_map)


# import ipdb; ipdb.set_trace()




"""
load urls.py

get view name and url name tuples

load test file

for each line:
    if its a test matching a view we have:
        on the next line add "url_name = [name]"
        
        
"""