#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re, ast

from my_scripting_library import ModelRunner

HOMEPATH = os.path.expanduser("~")

PROJECT_PATH = "{}/your/path".format(HOMEPATH)

MOMMY_PATH = os.path.join(PROJECT_PATH, "model_mommy")

MOMMY_TESTPATH = os.path.join(MOMMY_PATH, "tests")

# where your project settings module is located
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapi.server.project.settings")

def write_content(the_file, content):
    with open(the_file, 'w') as f:
        f.write(content)
        
def append_content(the_file, content):
    with open(the_file, 'a') as f:
        f.write(content)


MAIN_IMPORT = """\
from django.test import TestCase

from webapi.data.db.{app}.models import (
{models}
)

from model_mommy import mommy


"""


TEST_BLOCK = """\
class {base_model}MommyCreationTests(TestCase):
    
    def setUp(self):
        self.model = {base_model}

    def test_mommy_creates(self):
        x = mommy.make(self.model)
        x.save()
        self.assertTrue(x.pk)
    
    def test_mommy_isinstance(self):
        x = mommy.make(self.model)
        self.assertTrue(isinstance(x, self.model))


"""


def generate_models_import_statement(app_name, classes):
    
    def process_names(classes):
        starter = ",\n    ".join(classes)
        final = "    " + starter + "\n"
        final_without_extra_newline = final[:-1]
        return final_without_extra_newline
        
    processed_classes = process_names(classes)
    return MAIN_IMPORT.format(app=app_name, models=processed_classes)


def generate_test_writepath(app_name):
    filename = "test_" + app_name + "_mommy_creation.py"
    return os.path.join(MOMMY_TESTPATH, filename)


runner = ModelRunner(PROJECT_PATH)
for item in runner.model_names_per_app:
    app = item['app']
    names = item['model_names']
    writepath = generate_test_writepath(app)
    if names:
        try:
            import_statement = generate_models_import_statement(app, names)
            write_content(writepath, import_statement)
            for name in names:
                append_content(writepath, TEST_BLOCK.format(base_model=name))
        except IOError:
            pass