import datetime

import django_filters
from django.utils.timezone import utc

from webapi.data.db.dimensional.models import (
    Account, ActivePortfolioNumber, MinimalPortfolio, NetAssetValue, Portfolio, Trade,
)


class ActiveFilter(django_filters.Filter):
    """
    Requires table to have close and inception date fields
    """
    def filter(self, qs, value):
        current_date = datetime.datetime.now().replace(tzinfo=utc)
        if value == 'active':
            return qs.filter(close_date=None).exclude(inception_date_default__gte=current_date)
        elif value == 'closed':
            return qs.filter(close_date__lte=current_date)
        elif value == 'future':
            return qs.filter(inception_date_default__gte=current_date)
        return qs


class InFilter(django_filters.Filter):
    """
    Performs a Django __in query, taking in comma-separated values.

    For example, if defined as::

        id_in = InFilter(name='id')

    ...then you will be able to do a query such as::

        http://.../objects?id_in=10,11,12

    ...which will perform the Django query::

        .filter(id__in=[u'10',u'11',u'12'])
    """
    def __init__(self, *args, **kwargs):
        kwargs['lookup_type'] = 'in'
        super(InFilter, self).__init__(*args, **kwargs)

    def filter(self, qs, value):
        # Seems the version of django-filter we use treats values that are
        # list/tuples special, using first element as the value and the
        # second element as the lookup type.
        # Note that the master version of django-filter, however, seems to
        # handle things differently, checking for a Lookup instance instead of
        # a list/tuple.  So, this may need to be changed in the near future.
        value = (value.split(','), 'in')
        return super(InFilter, self).filter(qs, value)