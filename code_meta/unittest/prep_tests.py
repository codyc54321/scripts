#!/usr/bin/env python3
# coding: utf-8

import os, sys, subprocess, time, re

from ez_scrip_lib import *

if len(sys.argv) == 1:
    raise Exception('This script requires the path of the file to test, and the destination of the test file')
if len(sys.argv) == 2:
    raise Exception('Please also give the destination of the test file')

path = sys.argv[1]
destination = sys.argv[2]

path = ensure_absolute_path(path, CWD)
destination = ensure_absolute_path(destination, CWD)


ast_tree = get_ast_tree_for_file(path)

def generate_work_import_statement(path):
    path = path.replace(HOMEPATH, '').replace('work_projects', '').replace('core/unver', '').strip('/')                   \
            .lstrip('clientsite').lstrip('webapi').rstrip('.py').replace('/', '.').strip('.')
    return path
    
TEST_GENERATE_WORK_IMPORT_PATH_1 = '~/work_projects/webapi/webapi/tests/test_client/expectations/accounts.py'
result_1 = generate_work_import_statement(TEST_GENERATE_WORK_IMPORT_PATH_1)
assert result_1 == 'webapi.tests.test_client.expectations.accounts'

TEST_GENERATE_WORK_IMPORT_PATH_2 = '~/work_projects/core/unver/clientsite/clientsite/fundcenter/tests/test_stuff.py'
result_2 = generate_work_import_statement(TEST_GENERATE_WORK_IMPORT_PATH_2)
assert result_2 == 'clientsite.fundcenter.tests.test_stuff'