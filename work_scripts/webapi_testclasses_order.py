#!/usr/bin/env python

from ez_scrip_lib import *


WEBAPI_ROOT = "~/work_projects/webapi/webapi"

TEST_ROOT = join(WEBAPI_ROOT, "test_client")

LIVE_TESTS = join(TEST_ROOT, "test_live")

DEAD_TESTS = join(TEST_ROOT, "test_no_server")

MAIN = join(WEBAPI_ROOT, "client/portfolios.py")

def get_class_names_in_order(filepath):
    tree = get_ast_tree_for_file(filepath)
    return get_class_names(tree)


x = get_class_names_in_order(MAIN)
import ipdb; ipdb.set_trace()
print(x)
