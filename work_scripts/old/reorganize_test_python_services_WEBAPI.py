#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re

from ez_scrip_lib import get_classes_info_from_file, ensure_dir_exists, write_content, append_content, read_content

CLIENT_PATH = "~/work_projects/webapi/webapi/client"

CLIENT_TESTS_PATH = "~/work_projects/webapi/webapi/tests/test_client/test_python_services.py"

TMP_PATH = "~/scripts/tmp/testreorg"
ensure_dir_exists(TMP_PATH)

RELATIVE_CLIENT_PATH = "webapi/webapi/client"


def print_classnames(filepath):
    classes = get_classes_info_from_file(filepath)
    names = []
    for klass in classes:
        names.append(klass['name'])
    print "\nPath: ", filepath
    print("Classes:")
    print(names)
    
def retrieve_classnames(filepath):
    """retrieves {'filepath': the_filepath, 'classnames': [a, list, of, classnames]}"""
    classes = get_classes_infatasource_classnames
        names.append(klass['name'])
    d = {'filepath': filepath}
    d.update({'classnames': names})
    return d

def get_datasource_classnames():
    """
    returns a list of:
        [{'filepath': the_filepath, 'classnames': [a, list, of, classnames]},
         {'filepath': the_filepath, 'classnames': [a, list, of, classnames]},
         {'filepath': the_filepath, 'classnames': [a, list, of, classnames]}
        ]
    """
    classes_list = []
    for root, _, files in os.walk(CLIENT_PATH):
        for f in files:
            if f == '__init__.py' or '.pyc' in f:
                continue
            path = os.path.join(root, f)
            classes_list.append(retrieve_classnames(path))
        return classes_list
        
        
        
        
def get_filenames(path, banned_files=[]):
    """
    returns a list of:
        [{'filepath': the_filepath, 'classnames': [a, list, of, classnames]},
         {'filepath': the_filepath, 'classnames': [a, list, of, classnames]},
         {'filepath': the_filepath, 'classnames': [a, list, of, classnames]}
        ]
    """
    file_list = []
    for root, _, files in os.walk(path):
        for f in files:
            for string in banned_files:
                if string in f:
                    continue
            file_list.append(os.path.join(root, f))
    return file_list
        
        
        
        
        
    
    
    
    
def get_client_filename(filepath):
    removal_text = "~/work_projects/webapi/webapi/client"
    filename = filepath.replace(removal_text, "").replace(".py", "").replace("/", "")
    return filename
    
def join_classnames(classnames):
    return ",\n    ".join(classnames)
    
BASIC_IMPORTS = """\
import unittest
import datetime
from decimal import Decimal\n
"""

def generate_import_statement(filename, classes):
    start = "from webapi.client." + filename + " import "
    if len(classes) > 1:
        with_classes = start + "(\n    " + join_classnames(classes)
        final = with_classes + ",\n)\n\n"
        return final
    else:
        return start + classes[0] + "\n"
    
def get_potentional_datasource_classname(klass):
    return klass.replace("ClientTest", "")
    
def get_writepath(filename):
    f = "test_" + filename + ".py"
    return os.path.join(TMP_PATH, f)
    

def gut_testfile(test_info, datasource_classes):
    # for item in test_info:
    #     potentional_name = get_potentional_datasource_classname(item['name'])
    classes_to_remove = []
    for fileobject in datasource_classes:
        ds_path = fileobject['filepath']
        client_filename = get_client_filename(ds_path)
        writepath = get_writepath(client_filename)
        ds_classes = fileobject['classnames']
        if len(ds_classes):
            write_content(writepath, BASIC_IMPORTS)
            append_content(writepath, generate_import_statement(client_filename, ds_classes))
        for ds_classname in ds_classes:
            print ds_classname
            for item in test_info:
                potentional_datasource_classname = get_potentional_datasource_classname(item['name'])
                if ds_classname == potentional_datasource_classname:
                    print(item['class_text'])
                    append_content(writepath, "\n\n")
                    append_content(writepath, item['class_text'])
                    classes_to_remove.append(item['class_text'])
    test_content = read_content(CLIENT_TESTS_PATH)
    print test_content.count("\n")
    
    for c in classes_to_remove:
        test_content = test_content.replace(c, "")
        
    print test_content.count("\n")
    print(test_content)
    write_content(CLIENT_TESTS_PATH, test_content)

                    
datasource_classes = get_datasource_classnames()

test_class_info = get_classes_info_from_file(CLIENT_TESTS_PATH)
"""
name
filepath
class_text
start_to_stop
"""

gut_testfile(test_class_info, datasource_classes)



"""
PSEUDO

for class in the test file:
    if that classname - 'ClientTest' is a datasource from any client file:
        rip the test out and place it in a new test file named appropriately
        if the ds class it matches is found in 'distributions', this file should be named test_distributions.py
        this file should be put in a tmp folder for now
        




"""