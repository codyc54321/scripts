#!/usr/bin/env python
# coding: utf-8


import os, sys, subprocess, time, re, astunparse

from ez_scrip_lib import *

WEBAPI_PATH = os.path.join(HOMEPATH, 'work_projects/webapi/webapi')

SERVER_DB_PATH = os.path.join(WEBAPI_PATH, 'server/db')

TEST_PATH = os.path.join(WEBAPI_PATH, 'tests/test_client')

CLIENT_PATH = os.path.join(WEBAPI_PATH, 'client')

SILO_PATH = os.path.join(HOMEPATH, 'webapi_info')
ensure_dir_exists(SILO_PATH)

# ds = find_by_name('PortfoliosWithFixedHoldingsDataSource', ast_tree)

def process_name(name):
    return name.replace('ClientTest', '')


def parse_url_path(path_assignment_object):
    try:
        return path_assignment_object.value.s
    except:
        return path_assignment_object.value.left.s


def get_datasource_init(ds_name, filepath):
    """ feed the name as a string like EquityCountryWeightsDataSource """
    tree = get_ast_tree_for_file(filepath)
    ds = find_by_name(ds_name, tree)
    init = find_by_name('__init__', ds, type='function')
    return init
    
def get_datasource_path(init_ast_object):
    path = find_by_assignment_name('path', init_ast_object)
    url = parse_url_path(path)
    return url
    
    
def get_url_category(url):
    rgx = r"""[/](?P<category>\w+)[/](?P<target>\w+)"""
    match = re.search(rgx, url)
    return match.group('category'), match.group('target')
    

def get_filenames(path, banned_files=[]):
    """
    returns a list of:
        [{'filepath': the_filepath, 'classnames': [a, list, of, classnames]},
         {'filepath': the_filepath, 'classnames': [a, list, of, classnames]},
         {'filepath': the_filepath, 'classnames': [a, list, of, classnames]}
        ]
    """
    file_list = []
    for root, _, files in os.walk(path):
        for f in files:
            for string in banned_files:
                if string in f:
                    continue
            file_list.append(os.path.join(root, f))
        return file_list
        

def get_auto_tests(filepath):
        
    def is_auto_test(klass):
        try:
            func = find_by_name('test_data_retrieval', klass, type='function')
            if func:
                for item in func.body:
                    if isinstance(item, ast.Print):
                        if item.values[0].id == 'results':
                            return True
        except:
            return False
        
    try:
        auto_classes = []
        ast_tree = get_ast_tree_for_file(filepath)
        classes = parse_items(ast_tree, 'class')
        for klass in classes:
            if is_auto_test(klass):
                auto_classes.append(klass)
        return [klass.name for klass in auto_classes]
    except:
        return None


def determine_folder_path(test_path):
    """ takes full testpath and makes the folder structure """
    filename = get_filename_without_extension(test_path)
    path = os.path.join(SILO_PATH, filename)
    ensure_dir_exists(path)
    return path
    

def find_datasource_path_for_test(testname):
    ds_name = process_name(testname) 
    
    for root, _, files in os.walk(CLIENT_PATH):
        for f in files:
            try:
                path = os.path.join(root, f)
                ast_tree = get_ast_tree_for_file(path)
                classes = parse_items(ast_tree, 'class')
                for klass in classes:
                    if klass.name == ds_name:
                        return path #, astunparse.unparse(klass)
            except:
                pass
        return None

path = find_datasource_path_for_test('PortfolioCharacteristicsDataSource')
assert path == '~/work_projects/webapi/webapi/client/characteristics.py'


def write_datasource_info(testname, sourcepath, writepath):
    ds_name = process_name(testname)
    init_obj = get_datasource_init(ds_name, sourcepath)
    url = get_datasource_path(init_obj)
    print(get_url_category(url))
    try:
        init_string = astunparse.unparse(init_obj)
    except:
        init_string = ''
    intro = 'For {} in {}'.format(testname, sourcepath)
    append_content(writepath, intro)files
    append_content(writepath, '\n\ninit signature:\n\n')
    append_content(writepath, init_string)
    append_content(writepath, '\n\n-----------------------------------------')
    
    

def write_info_for_testfile(filepath):
    folder_path = determine_folder_path(filepath)
    classes = get_auto_tests(filepath)
    if classes:
        for klass in classes: # these are test classes
            writepath = os.path.join(folder_path, klass+'.txt')
            write_content(writepath, '')
            datasource_path = find_datasource_path_for_test(klass)
            write_datasource_info(klass, datasource_path, writepath)
    
    
test_files = get_filenames(TEST_PATH, ['constants', '__init__', '.pyc'])
for f in test_files:
    write_info_for_testfile(f)
    
    
classes = get_auto_tests('~/work_projects/webapi/webapi/tests/test_client/test_portfolios.py')



    
    
""" 
grab class name first (remove ClientTest from the classname)

check for automated test (look for print(results) expression)

if autogenerated test, find datasource file and the class definition

find the urls file based on the 'path' attribute for the ds class:

    find the urls file, for /dimensional/portfolio_identifier/%s/' % portfolio_id we have WEBAPI_PATH/server/db/dimensional/urls.py
    
    find the view used in that url
    
find the file holding the view

find the model the view uses
"""