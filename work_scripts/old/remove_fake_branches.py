#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re

from ez_scrip_lib import *

WEBAPI_ROOT = "~/work_projects/webapi"

D = {'cwd': WEBAPI_ROOT}

DATA = [
    {'original': "ckc/webapi-tests-returns-views-WEBAPI-212", 'module': 'returns', 'ticket': '212'},
    {'original': "ckc/webapi-tests-marketing-views-WEBAPI-214", 'module': 'marketing', 'ticket': '214'},
    {'original': "ckc/webapi-tests-attributions-views-WEBAPI-215 ", 'module': 'attributions', 'ticket': '215'},
    {'original': "ckc/webapi-tests-fixed-income-views-WEBAPI-217", 'module': 'fixed_income', 'ticket': '217'},
    {'original': "ckc/webapi-tests-dimensional-views-WEBAPI-218", 'module': 'dimensional', 'ticket': '218'},
    
    {'original': "ckc/webapi-tests-fixed-income-views-WEBAPI-217", 'module': 'characteristics', 'ticket': '238'}, # has all files
    {'original': "ckc/webapi-tests-fixed-income-views-WEBAPI-217", 'module': 'fees', 'ticket': '237'}, # has all files
    {'original': "ckc/webapi-tests-fixed-income-views-WEBAPI-217", 'module': 'accounts', 'ticket': '239'}, # has all files
]

def call_git(command, cwd=D):
    git_command = "git " + command
    return call_sp(git_command, **D)

def get_general_fake_branch_name(module):
    return "ckc/fake-webapi-tests-{module}-views-WEBAPI-999".format(module=module)

def get_specific_fake_branch_name(module, ticket):
    return "ckc/fake-webapi-tests-{module}-views-WEBAPI-{ticket}".format(module=module, ticket=ticket)

"""reset"""
for item in DATA:
    module = item['module']
    original_branch = item['original']
    ticket = item['ticket']
    
    this_branch = get_general_fake_branch_name(module)
    # print(this_branch)
    call_git("push origin --delete {}".format(this_branch))
    call_git("branch -D {}".format(this_branch))
     
    now_this_branch = get_specific_fake_branch_name(module, ticket)
    # print(now_this_branch)
    call_git("push origin --delete {}".format(now_this_branch))
    call_git("branch -D {}".format(now_this_branch))