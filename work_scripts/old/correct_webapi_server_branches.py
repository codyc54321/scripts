#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re

from ez_scrip_lib import *

rgx = r"(?P<name>\w+)\s*[=]"

WEBAPI_ROOT = "~/work_projects/webapi"

TEST_CLIENT_ROOT = join(HOMEPATH, "work_projects/webapi/webapi/tests/test_client")
 
CLIENT_PATH = join(WEBAPI_ROOT, "webapi/client")

SERVER_PATH = join(WEBAPI_ROOT, "webapi/server")

LOCAL_TESTS_PATH = join(WEBAPI_ROOT, "webapi/tests_localdb")

LOCAL_SERVER_TESTS_PATH = join(LOCAL_TESTS_PATH, "test_server")

LOCAL_TESTS_UTILS_PATH = join(LOCAL_TESTS_PATH, "utils.py")

D = {'cwd': WEBAPI_ROOT}

DATA = [
    {'original': "ckc/webapi-tests-returns-views-WEBAPI-212", 'module': 'returns', 'ticket': '212'},
    {'original': "ckc/webapi-tests-marketing-views-WEBAPI-214", 'module': 'marketing', 'ticket': '214'},
    {'original': "ckc/webapi-tests-attributions-views-WEBAPI-215 ", 'module': 'attributions', 'ticket': '215'},
    {'original': "ckc/webapi-tests-fixed-income-views-WEBAPI-217", 'module': 'fixed_income', 'ticket': '217'},
    {'original': "ckc/webapi-tests-dimensional-views-WEBAPI-218", 'module': 'dimensional', 'ticket': '218'},
    
    {'original': "ckc/webapi-tests-fixed-income-views-WEBAPI-217", 'module': 'characteristics', 'ticket': '238'}, # has all files
    {'original': "ckc/webapi-tests-fixed-income-views-WEBAPI-217", 'module': 'fees', 'ticket': '237'}, # has all files
    {'original': "ckc/webapi-tests-fixed-income-views-WEBAPI-217", 'module': 'accounts', 'ticket': '239'}, # has all files
]

MODULES = ['returns', 'marketing', 'attributions', 'fixed_income', 'dimensional',
           'characteristics', 'fees', 'accounts']

EXAMPLE_BRANCH = 'ckc/webapi-tests-attributions-views-WEBAPI-215'

def call_git(command, cwd=D):
    git_command = "git " + command
    return call_sp(git_command, **D)

def update_branch(branch):
    call_git("checkout {}".format(branch))
    call_git("pull")

def get_new_branch_name(module, ticket):
    return "ckc/{module}-views-tests-WEBAPI-{ticket}".format(module=module, ticket=ticket)


"""reset"""
# for item in DATA:
#     module = item['module']
#     original_branch = item['original']
#     ticket = item['ticket']
#     old_branch = get_new_branch_name(module, ticket)
#     call_git("push origin --delete {}".format(old_branch))
#     call_git("branch -D {}".format(old_branch))
     
     
def initialize_branch(branch):
    update_branch("master")
    call_git("checkout -b {}".format(branch))
    call_git("push -u origin {}".format(branch))
    
def get_file_content(branch, filepath):
    update_branch(branch)
    return read_content(filepath)
    
def commit_existing_file(target_branch, filepath, message):
    filepath = filepath.replace(WEBAPI_ROOT, '').lstrip('/')
    call_git("checkout {}".format(target_branch))
    call_git("checkout {} -- {}".format(EXAMPLE_BRANCH, filepath)) # ckc/webapi-tests-attributions-views-WEBAPI-215
    call_git("add {}".format(filepath))
    call_git('commit -m "{}"'.format(message))
    call_git("push")
    
def get_possible_client_filepath(module):
    return join(WEBAPI_ROOT, "webapi/client/{module}.py".format(module=module))
    
def get_possible_server_db_urls_path(module):
    return join(WEBAPI_ROOT, "webapi/server/db/{module}/urls.py".format(module=module))

def get_possible_server_db_views_path(module):
    return join(WEBAPI_ROOT, "webapi/server/db/{module}/views.py".format(module=module))

def get_possible_server_test_path(module):
    path = "webapi/tests_localdb/test_server/test_{module}.py".format(module=module)
    print(path)
    return path
    
def has_diff(branch, filepath):
    try:
        content_1 = get_file_content(branch, filepath)
        content_2 = get_file_content("master", filepath)
        return not content_the_same(content_1, content_2)
    except IOError:
        return False
    # diff = call_git("--no-pager diff {} {} -- {} | cat".format(branch_1, branch_2, filepath))
    
def has_client_diff(module):
    possible_path = get_possible_client_filepath(module)
    has_it = has_diff(EXAMPLE_BRANCH, possible_path)
    return has_it
    
def has_urls_diff(module):
    # if module == 'marketing':
    #     import ipdb; ipdb.set_trace()
    urls_path = get_possible_server_db_urls_path(module)
    has_it = has_diff(EXAMPLE_BRANCH, urls_path)
    return has_it
    
def has_views_diff(module):
    views_path = get_possible_server_db_views_path(module)
    has_it = has_diff(EXAMPLE_BRANCH, views_path)
    return has_it
    
def has_local_tests_diff(original_branch, module):
    tests_path = get_possible_server_test_path(module)
    print(tests_path)
    has_it = has_diff(original_branch, tests_path)
    print(has_it)
    return has_it

def update_file_maybe(target_branch, module, diff_callback, path_callback, message, test_branch=None):
    if module == 'fees' and test_branch:
        import ipdb; ipdb.set_trace()
    if test_branch:
        has_it = diff_callback(test_branch, module)
    else:
        has_it = diff_callback(module)
    if has_it:
        path = path_callback(module)
        commit_existing_file(target_branch, path, message)


for item in DATA:
    module = item['module']
    original_branch = item['original']
    ticket = item['ticket']
    new_branch = get_new_branch_name(module, ticket)
    initialize_branch(new_branch)
    update_file_maybe(new_branch, module, has_client_diff, get_possible_client_filepath, "updating {} client".format(module))
    update_file_maybe(new_branch, module, has_urls_diff, get_possible_server_db_urls_path, "updating server/db/{}/urls.py".format(module))
    update_file_maybe(new_branch, module, has_views_diff, get_possible_server_db_views_path, "updating server/db/{}/views.py".format(module))
    possible_test_path = get_possible_server_test_path(module)
    message = "adding local server tests for {}".format(module)
    print(message)
    if module == 'accounts':
        import ipdb; ipdb.set_trace()
    commit_existing_file(new_branch, possible_test_path, message)    
    commit_existing_file(new_branch, LOCAL_TESTS_UTILS_PATH, "adding utils")

"""
for each item in data:
    checkout master
    checkout a new branch named "ckc-fake-{module_name}-server-tests-remake"
    git push -u origin branchname 
    (always pull from the original branchname, found in data['branch'])
    sa
    if a client matching the module was changed (webapi/webapi/client/fees.py), pull that file
    commit "updated client"
    
    if webapi/server/db/ is changed {marketing,dimensional} add those files (urls and views) and commit
    
    commit 
    
    checkout a file in webapi/tests_localdb/test_server if it matches the module name
    
    commit
    
    
"""