#!/usr/bin/env python
# coding: utf-8


import os, sys, subprocess, time, re, ast

from astunparse import unparse

from ez_scrip_lib import (
    get_classes_info_from_file, ensure_dir_exists, write_content, append_content, read_content, join, get_filename,
    ClassASTHandler, FunctionASTHandler, get_ast_tree_for_file, parse_items
)

CLIENTSITE_BASE = "~/work_projects/webapi/webapi"

CLIENT_PATH = join(CLIENTSITE_BASE, "client")

CLIENT_TESTS_PATH = join(CLIENTSITE_BASE, "tests/test_client")

EXPECTATIONS_PATH  = join(CLIENT_TESTS_PATH, "expectations")


def get_expectation_from_function(function_tree):
    handler = FunctionASTHandler(function_tree)
    expectation = handler.get_assignment_by_name('expect')
    return expectation
    
    
def get_expectations_from_class(class_tree):
    assert isinstance(class_tree, ast.ClassDef)
    handler = ClassASTHandler(class_tree)
    functions = handler.functions
    expectations = []
    for function in functions:
        fhandler = FunctionASTHandler(function)
        expectation = fhandler.get_assignment_by_name('expect')
        if expectation:
            class_name = handler.name
            function_name = fhandler.name
            expectation_name = class_name + '_' + function_name + '_expectation'
            expectations.append({'name': expectation_name, 'code': unparse(expectation.value)})
    return expectations

def get_expectations_from_file(filepath):
    expectations = []
    file_tree = get_ast_tree_for_file(filepath)
    classes = parse_items(file_tree, 'class')
    for klass in classes:
        these_expectations = get_expectations_from_class(klass)
        if these_expectations:
            expectations.extend(these_expectations)
    return expectations
    
    
def get_expectation_filename(filepath):
    filename = get_filename(filepath)
    filename = filename.replace('test_', '')
    return join(EXPECTATIONS_PATH, filename)
    
assert get_expectation_filename(join(CLIENT_TESTS_PATH, "test_fees.py")) == '~/work_projects/webapi/webapi/tests/test_client/expectations/fees.py'


def join_expectation_text(d):
    """takes {'name': ..., 'code': ...}"""
    return d['name'] + " = " + d['code'] + '\n\n\n'
    
    
def write_expectations_for_file(filepath):
    expectation_filename = get_expectation_filename(filepath)
    expectations = get_expectations_from_file(filepath)
    for expectation in expectations:
        code = join_expectation_text(expectation)
        append_content(expectation_filename, code)
    
        
def write_expectations(startpath):
    for root, _, files in os.walk(startpath):
        for f in files:
            fullpath = os.path.join(root, f)
            write_expectations_for_file(fullpath)

write_expectations_for_file(join(CLIENT_TESTS_PATH, "test_distributions.py"))
    
        
"""
PSEUDO

for each class in the file:
    for each function in that class:
        look for an expectation
        
        if you find one, create a name for it like {class}_{the test function}_expectation  <-- **here**
        
        write the expectation to an appropriate file named after the module, using the generated expectation name
        
        *maybe?
        rewrite class based on AST without the expectation in it


"""