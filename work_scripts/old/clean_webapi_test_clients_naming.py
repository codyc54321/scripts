#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re

from ez_scrip_lib import *

# filepath = sys.argv[1]

rgx = r"(?P<name>\w+)\s*[=]"

WEBAPI_ROOT = "~/work_projects/webapi"

TEST_CLIENT_ROOT = "~/work_projects/webapi/webapi/tests/test_client"
 
CONSTANTS_PATH = join(TEST_CLIENT_ROOT, "constants.py")

EXPECTATIONS_PATH = join(TEST_CLIENT_ROOT, "expectations")

DATA = [
    {'branch': "ckc/split-attributions-tests-out-WEBAPI-225", 'modules': ['attributions']},
    {'branch': "ckc/split-distributions-tests-out-WEBAPI-228", 'modules': ['distributions']},
    {'branch': "ckc/split-fees-tests-out-WEBAPI-229", 'modules': ['fees']},
    {'branch': "ckc/split-fixed-tests-out-WEBAPI-230", 'modules': ['fixed']},
    {'branch': "ckc/split-portfolios-tests-out-WEBAPI-232", 'modules': ['portfolios']},
    {'branch': "ckc/split-vehicles-and-saleslogix-tests-out-WEBAPI-234", 'modules': ['vehicles', 'saleslogix_client']},
]


def get_names(filepath):
    lines = read_lines(filepath)
    names = []
    for line in lines:
        match = re.search(rgx, line)
        if match:
            name = match.group('name')
            names.append(name.strip())
    return names


def get_old_expectations_path(module_name):
    filename = module_name + ".py"
    return join(TEST_CLIENT_ROOT, 'expectations', filename)


def get_correct_expectations_path(module_name):
    filename = module_name + "_expectations.py"
    return join(TEST_CLIENT_ROOT, 'expectations', filename)
    

def rename_expectaions_path_and_commit(module_name):
    old_path = get_old_expectations_path(module_name)
    new_path = get_correct_expectations_path(module_name)
    os.rename(old_path, new_path)
    call_sp("git add {old} {new}".format(old=old_path, new=new_path))
    call_sp('git commit -m "correct expectations file name"')
    call_sp('git push')


def get_these_expectations(module_name):
    """ only works after renaming..."""
    filepath = get_correct_expectations_path(module_name)
    expectations = get_names(filepath)
    return expectations

def get_test_path(filename):
    f = 'test_' + filename + '.py'
    return join(TEST_CLIENT_ROOT, f)


def file_has_a_name(content, names):
    for name in names:
        if name in content:
            return True
    return False
    

def fix_constants_import_statements(filepath, constants):
    content = read_content(filepath)
    correct_import_statement = "from webapi.tests.test_client import constants\n"
    bad_imports = [
        "from webapi.tests.test_client.constants import *\n",
        "from .constants import *\n",
    ]
    for statement in bad_imports:
        if statement in content:
            content = content.replace(statement, correct_import_statement)
    if not file_has_a_name(content, constants):
        print("WARNING: had to remove constants import...")
        time.sleep(2)
        content = content.replace(correct_import_statement, '')
        write_content(filepath, content)
        return False
    write_content(filepath, content)


def fix_expectations_import_statements(filepath, module, expectations): # module = fees, fixed, etc
    content = read_content(filepath)
    correct_import_statement = "from webapi.tests.test_client.expectations import {module}_expectations\n".format(module=module)
    bad_imports = [
        "from webapi.tests.test_client.expectations.{module} import *\n".format(module=module), 
        "from .expectations.{module} import *\n".format(module=module),
    ]
    for statement in bad_imports:
        if statement in content:
            content = content.replace(statement, correct_import_statement)
    if not file_has_a_name(content, expectations):
        print("WARNING: had to remove expectations import...")
        time.sleep(2)
        try:
            content = content.replace(correct_import_statement, '')
        except:
            pass
        return False
    write_content(filepath, content)


def substitute_names(filepath, names, module_name):
    content = read_content(filepath)
    for name in names:
        if name in content:
            replacement = module_name + '.' + name
            content = content.replace(name, replacement)
    write_content(filepath, content)


def substitute_constants_names(filepath, names, module_name):
    for name in names:
        content = read_content(filepath)
        rgx = r"\b{}\b".format(name)
        matches = re.finditer(rgx, content)
        new_content = ""
        current_start = 0
        for item in matches:
            current_end = item.start()
            
            new_name = "constants." + name
            new_content += content[current_start:current_end]
            new_content += new_name
            
            current_start = item.end()
        new_content += content[current_start:]
        write_content(filepath, new_content)


def commit_file(filepath, message, git_data={'cwd': WEBAPI_ROOT}):
    call_sp('git commit {file_to_commit} -m "{message}"'.format(file_to_commit=filepath, message=message), **git_data)
    

def run():
    
    git_data = {'cwd': WEBAPI_ROOT}
    
    for d in DATA:
        branch = d['branch']
        modules = d['modules']
        
        call_sp("git checkout {}".format(branch))
        
        call_sp('git pull', **git_data)
        
        constants = get_names(CONSTANTS_PATH)
        
        expecations_init_path = join(EXPECTATIONS_PATH, "__init__.py")
        if not os.path.exists(expecations_init_path):
            call_sp("touch {}".format(expecations_init_path))
            commit_file(expecations_init_path, "added init file to expectations for importing")
            call_sp('git push')
        
        for module in modules:
            
            testpath = get_test_path(module)
            test_file_content = read_content(testpath)
                        
            if 'constant' in test_file_content:
                fix_constants_import_statements(testpath, constants)
                test_file_content = read_content(testpath)
                substitute_constants_names(testpath, constants, 'constants')
            
            
            theoretical_expectation_path = get_old_expectations_path(module)
            
            if os.path.exists(theoretical_expectation_path):
                rename_expectaions_path_and_commit(module)
                these_expectations = get_these_expectations(module)
                print('these_expectations')
                print(these_expectations)
                fix_expectations_import_statements(testpath, module, these_expectations)
                test_file_content = read_content(testpath)
                substitute_names(testpath, these_expectations, (module + "_expectations"))
            else:
                print("\nNOTE: There are no expectations here")

            commit_file(testpath, "corrected import format for test_{}".format(module))
            call_sp("git push")

# run()


call_sp('git co ckc/split-datasource-tests-out-WEBAPI-227')
constants = get_names(CONSTANTS_PATH)
PATH = join(TEST_CLIENT_ROOT, "test_datasource.py")
content = read_content(PATH)
does_it = file_has_a_name(content, constants)


"""
load file of interest content


if 'constant' in file content:
    
    clean constants imports
    
    for each name in constants:
        if name is in this file, replace with constants.name
    

if 'expectation' in file content:
    
    clean expectations imports
    
    for each name in expectations:
        if name is in this file, replace with expectations.name


commit file changes
"""
            
