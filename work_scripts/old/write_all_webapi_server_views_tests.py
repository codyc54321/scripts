#!/usr/bin/env python
# coding: utf-8


import os, sys, subprocess, time, re

from ez_scrip_lib import *

        
"""
get the class names for the file

create file in tests/test_server/filename

make a test class for each class:

    if the test subclasses ReadOnlyModelViewSet, 
    make a few basic tests that it can retrieve data from database
    
    else if it's a normal APIView, try to fill in some basic info of the get signature and start
    a test off which retrieves the results, like for
    
    class HoldingHistoricalView(APIView):

        def get(self, request, account_id, format=None):
    
            return Response({
                'query_items': {
                    'account_id': account_id,
                    'date': holdings_date,
                },
                'results': data,
            })
            
    prefill a test like
    
    def test_data_retrieval:
        view_instance = {api_class}()
        results = view_instance.get(self.RequestFactory, ACCOUNT_ID, FORMAT=None)
        for item in HoldingHistoricalView_test_data_retrieval_expectations:
            self.assertIn(item, results)

"""


IMPORTS = """\
import json

from django.test import TestCase, Client

from rest_framework.test import APIRequestFactory

"""

def generate_classnames_import_statement(import_path, classnames):
    def join_classnames(classnames):
        start = '    '
        middle = ",\n    ".join(classnames)
        end = ""
        return start + middle + end
            
    block = """\
from {import_path} import (
{classes}
)

"""
    joined_classnames = join_classnames(classnames)
    return block.format(import_path=import_path, classes=joined_classnames)



TEST_BLOCK = """\
class {view}Tests(TestCase):
    
    def setUp(self):
        self.request_factory = APIRequestFactory()
        self.view = {view}()

    def {testname}(self):
        response = self.view.get(self.request_factory)
        results = response['results']
        for item in {expectation}:
            self.assertIn(item, results)
        

"""

EXPECTATION_IMPORTS = """\
from decimal import Decimal

\"\"\"
Test expectations can be found by the format {testclass}_{test_function}_expectations
\"\"\"\n\n
"""

WEBAPI_BASE = join(HOMEPATH, 'work_projects/webapi')
BASE_SERVER_PATH = join(HOMEPATH, 'work_projects/webapi/webapi/server')
BASE_SERVER_DB_PATH = join(HOMEPATH, 'work_projects/webapi/webapi/server/db')
TEST_PATH = join(HOMEPATH, 'work_projects/webapi/webapi/tests/test_server/views')
EXPECTATION_PATH = join(TEST_PATH, 'expectations')
    
    
def generate_testname(klass):
    # DividendMaxExDateView as input
    words = re.findall('[A-Z][^A-Z]*', klass) # ['Dividend', 'Max', 'Ex', 'Date', 'View']
    words = [word.lower() for word in words if word.lower() != 'view'] # ['dividend', 'max', 'ex', 'date']
    testname = "_".join(words)
    return 'test_' + testname + '_get'
    
assert generate_testname('DividendMaxExDateView') == 'test_dividend_max_ex_date_get'

def generate_expectation_name(view):
    return view + 'Tests_' + generate_testname(view) + '_expectations'
    
def generate_expectation_block(name):
    return name + ' = [\n\n]\n\n'

def retrieve_expectations(classnames):
    blocks = [EXPECTATION_IMPORTS]
    for name in classnames:
        expectation_name = generate_expectation_name(name)
        blocks.append(generate_expectation_block(expectation_name))
    return blocks

def generate_expectation_path(app_name):
    return os.path.join(EXPECTATION_PATH, app_name + '_expectations.py')
    
def generate_expectation_import_path(app):
    return 'webapi.tests.test_server.views.expectations.' + app + '_expectations'
    
def get_classes_from_file(filepath):
    ast = get_ast_tree_for_file(filepath)
    return parse_items(ast, 'class')

def get_classnames(filepath):
    classes = get_classes_from_file(filepath)
    all_names = [klass.name for klass in classes]
    final_names = filter(lambda x: x != "Meta", all_names)
    return final_names
    
def get_top_dir(filepath):
    head, tail = os.path.split(filepath)
    if head in ['', '/', '/home', '/home/' '~']:
        return tail
    else:
        return get_top_dir(head)

def get_app_name_from_filepath(filepath):
    relpath = filepath.replace(BASE_SERVER_DB_PATH, '').replace(BASE_SERVER_PATH, '')
    return get_top_dir(relpath)

def determine_import_path(filepath, package_dir):
    # imports are relative from the package they're in
    relpath = filepath.replace(package_dir, '')
    import_path = relpath.lstrip('/').rstrip('.py').replace('/', '.')
    return import_path
    
def generate_test_block(view, app, expectation):
    testname = generate_testname(view)
    return TEST_BLOCK.format(view=view, app=app, expectation=expectation, testname=testname)

def retrieve_test_blocks(classnames, app):
    blocks = []
    for name in classnames:
        expectation_name = generate_expectation_name(name)
        blocks.append(generate_test_block(name, app, expectation_name))
    return blocks
    
def is_view_file(filepath):
    return filepath.endswith('views.py')
    
def generate_testpath(filepath, app_name):
    return os.path.join(TEST_PATH, 'test_' + app_name + '.py')


# relpath = sys.argv[1]
# filepath = join(CWD, relpath)
# content = read_content(filepath)
    
filepath = os.path.join(BASE_SERVER_DB_PATH, 'dimensional/views.py')
    
assert is_view_file(filepath) == True

# classes = get_classes_from_file(filepath)
# latest_nav = ClassASTHandler(classes[9])
# get_func = latest_nav.find_function_by_name('get')

def write_all_content(filepath, blocks):
    write_content(filepath, blocks[0])
    for block in blocks[1:]:
        append_content(filepath, block)

def write_a_testfile(filepath):
    classnames = get_classnames(filepath)
    
    app_name = get_app_name_from_filepath(filepath)
    
    import_path = determine_import_path(filepath, WEBAPI_BASE)
    
    views_import_statement = generate_classnames_import_statement(import_path, classnames)
    
    expectation_names = [generate_expectation_name(klass) for klass in classnames]
    expectation_import_path = generate_expectation_import_path(app_name)
    expectation_import_statement = generate_classnames_import_statement(expectation_import_path, expectation_names)
    
    blocks = [IMPORTS, views_import_statement, expectation_import_statement, '\n']
    writepath = generate_testpath(filepath, app_name)
    code_blocks = retrieve_test_blocks(classnames, app_name)
    blocks.extend(code_blocks)
    write_all_content(writepath, blocks)
    
    expectation_path = generate_expectation_path(app_name)
    expectation_blocks = retrieve_expectations(classnames)
    write_all_content(expectation_path, expectation_blocks)
    
    

for root, _, files in os.walk(BASE_SERVER_PATH):
    for f in files:
        filepath = os.path.join(root, f)
        if is_view_file(filepath):
            # print(filepath)
            # print(get_app_name_from_filepath(filepath))
            # print(testpath)
            # print('\n')
            write_a_testfile(filepath)