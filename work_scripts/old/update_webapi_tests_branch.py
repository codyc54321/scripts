#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re

from ez_scrip_lib import *

rgx = r"(?P<name>\w+)\s*[=]"

WEBAPI_ROOT = "~/work_projects/webapi"

TESTS_PATH = join(HOMEPATH, "work_projects/webapi/webapi/tests")

TEST_CLIENT_ROOT = join(TESTS_PATH, "test_client")

TESTS_UTILS_PATH = join(TESTS_PATH, "utilities/__init__.py")
 
CLIENT_PATH = join(WEBAPI_ROOT, "webapi/client")

SERVER_PATH = join(WEBAPI_ROOT, "webapi/server")

LOCAL_TESTS_PATH = join(WEBAPI_ROOT, "webapi/tests_localdb")

LOCAL_TESTS_SETTINGS_PATH = join(LOCAL_TESTS_PATH, "settings.py")

LOCAL_SERVER_TESTS_PATH = join(LOCAL_TESTS_PATH, "test_server")

D = {'cwd': WEBAPI_ROOT}

DATA = [
        {'module': 'marketing', 'ticket': '214'},
        {'module': 'fixed_income', 'ticket': '217'},
        {'module': 'dimensional', 'ticket': '218'},
        
        {'module': 'characteristics', 'ticket': '238'}, # has all files
        {'module': 'fees', 'ticket': '237'}, # has all files
        {'module': 'accounts', 'ticket': '239'}, # has all files 
        ]

EXAMPLE_BRANCH = "ckc/returns-views-tests-WEBAPI-212"


def call_git(command, cwd=D):
    git_command = "git " + command
    return call_sp(git_command, **D)

def update_branch(branch):
    call_git("checkout {}".format(branch))
    call_git("pull")

def get_new_branch_name(module, ticket):
    return "ckc/{module}-views-tests-WEBAPI-{ticket}".format(module=module, ticket=ticket)
          
def get_file_content(branch, filepath):
    update_branch(branch)
    return read_content(filepath)
    
def commit_existing_file(target_branch, filepath, message):
    filepath = filepath.replace(WEBAPI_ROOT, '').lstrip('/')
    print('inside commit_existing_file:')
    print(target_branch)
    print(filepath)
    print('\n')
    call_git("checkout {}".format(target_branch))
    call_git("checkout {} -- {}".format(EXAMPLE_BRANCH, filepath)) # ckc/webapi-tests-attributions-views-WEBAPI-215
    call_git("add {}".format(filepath))
    call_git('commit -m "{}"'.format(message))
    call_git("push")
    
def get_possible_client_filepath(module):
    return join(WEBAPI_ROOT, "webapi/client/{module}.py".format(module=module))
    
def get_possible_server_db_urls_path(module):
    return join(WEBAPI_ROOT, "webapi/server/db/{module}/urls.py".format(module=module))

def get_possible_server_db_views_path(module):
    return join(WEBAPI_ROOT, "webapi/server/db/{module}/views.py".format(module=module))

def get_possible_server_test_path(module):
    path = "webapi/tests_localdb/test_server/test_{module}.py".format(module=module)
    print(path)
    return path
    
def has_diff(branch, filepath):
    try:
        content_1 = get_file_content(branch, filepath)
        content_2 = get_file_content("master", filepath)
        return not content_the_same(content_1, content_2)
    except IOError:
        return False
    # diff = call_git("--no-pager diff {} {} -- {} | cat".format(branch_1, branch_2, filepath))
    
def has_client_diff(module):
    possible_path = get_possible_client_filepath(module)
    has_it = has_diff(EXAMPLE_BRANCH, possible_path)
    return has_it
    
def has_urls_diff(module):
    # if module == 'marketing':
    #     import ipdb; ipdb.set_trace()
    urls_path = get_possible_server_db_urls_path(module)
    has_it = has_diff(EXAMPLE_BRANCH, urls_path)
    return has_it
    
def has_views_diff(module):
    views_path = get_possible_server_db_views_path(module)
    has_it = has_diff(EXAMPLE_BRANCH, views_path)
    return has_it
    
def has_local_tests_diff(original_branch, module):
    tests_path = get_possible_server_test_path(module)
    print(tests_path)
    has_it = has_diff(original_branch, tests_path)
    print(has_it)
    return has_it

def update_file_maybe(target_branch, module, diff_callback, path_callback, message, test_branch=None):
    if module == 'fees' and test_branch:
        import ipdb; ipdb.set_trace()
    if test_branch:
        has_it = diff_callback(test_branch, module)
    else:
        has_it = diff_callback(module)
    if has_it:
        path = path_callback(module)
        commit_existing_file(target_branch, path, message)


module = DATA['module']
ticket = DATA['ticket']
new_branch = get_new_branch_name(module, ticket)
print('new_branch', new_branch)
# update_file_maybe(new_branch, module, has_client_diff, get_possible_client_filepath, "updating {} client".format(module))
update_file_maybe(new_branch, module, has_urls_diff, get_possible_server_db_urls_path, "updating server/db/{}/urls.py".format(module))
# update_file_maybe(new_branch, module, has_views_diff, get_possible_server_db_views_path, "updating server/db/{}/views.py".format(module))
possible_test_path = get_possible_server_test_path(module)
message = "adding local server tests for {}".format(module)
print(message)
commit_existing_file(new_branch, possible_test_path, message)
commit_existing_file(new_branch, TESTS_UTILS_PATH, "adding get_response_data()")
commit_existing_file(new_branch, LOCAL_TESTS_SETTINGS_PATH, "updating local tests settings")

