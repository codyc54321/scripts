#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re

from ez_scrip_lib import *

rgx = r"(?P<name>\w+)\s*[=]"

WEBAPI_ROOT = "~/work_projects/webapi"

# TOX_PATH = join(WEBAPI_ROOT, "tox.ini")
UTILS_PATH = join(WEBAPI_ROOT, "webapi/tests_localdb/utils.py")


D = {'cwd': WEBAPI_ROOT}

DATA = [
    {'module': 'accounts', 'ticket': '239'},
    {'module': 'characteristics', 'ticket': '238'},
    # {'module': 'dimensional', 'ticket': '218'},
    {'module': 'fees', 'ticket': '237'},
    {'module': 'fixed_income', 'ticket': '217'},
    {'module': 'marketing', 'ticket': '214'},
    {'module': 'returns', 'ticket': '212'},
]

EXAMPLE_BRANCH = "ckc/dimensional-views-tests-WEBAPI-218"


def call_git(command, cwd=D):
    git_command = "git " + command
    return call_sp(git_command, **D)

def update_branch(branch):
    call_git("checkout {}".format(branch))
    call_git("pull")

def get_new_branch_name(module, ticket):
    return "ckc/{module}-views-tests-WEBAPI-{ticket}".format(module=module, ticket=ticket)

def get_all_branches(data):
    branches = []
    for item in data:
        module = item['module']
        ticket = item['ticket']
        new_branch = get_new_branch_name(module, ticket)
        branches.append(new_branch)
    return branches

def get_file_content(branch, filepath):
    update_branch(branch)
    return read_content(filepath)

def commit_existing_file(target_branch, filepath, message):
    filepath = filepath.replace(WEBAPI_ROOT, '').lstrip('/')
    call_git("checkout {}".format(target_branch))
    call_git("checkout {} -- {}".format(EXAMPLE_BRANCH, filepath))
    call_git("add {}".format(filepath))
    call_git('commit -m "{}"'.format(message))
    call_git("push")

for branch in get_all_branches(DATA):
    print(branch)
    commit_existing_file(branch, UTILS_PATH, "add clear_tables()")
