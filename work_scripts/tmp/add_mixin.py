#!/usr/bin/env python
# coding: utf-8


import os, sys, subprocess, time, re

from ez_scrip_lib import *

WEBAPI_ROOT = "~/work_projects/webapi"

TESTFILE_PATH = join(WEBAPI_ROOT, 'webapi/tests_localdb/test_server/test_dimensional.py')

testfile_content= read_content(TESTFILE_PATH)


new_content = re.sub(r"TestCase", 'TestCase, SchemaValidatorMixin', testfile_content)

write_content(TESTFILE_PATH, new_content)



