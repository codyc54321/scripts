#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re, ast, codegen

from ez_scrip_lib import *



# parsed = ast.parse(read_content('~/work_projects/webapi/webapi/tests/test_client/test_portfolios.py'))

def parse_classes(parsed_file):
    return [klass for klass in ast.walk(parsed_file) if isinstance(klass, ast.ClassDef)]

def parse_functions(code_object):
    return [func for func in ast.walk(code_object) if isinstance(func, ast.FunctionDef)]

def parse_assignments(code_object):
    return [assignment for assignment in ast.walk(code_object) if isinstance(assignment, ast.Assign)]

# classes = parse_classes(parsed)
# 
# klass = classes[0]


def get_test_funcs_in_class(klass):
    funcs = parse_functions(klass)
    relevant_funcs = [func for func in funcs if 'test' in func.name]
    return relevant_funcs

def get_expected_for_func(func):
    assignments = parse_assignments(func)
    try:
        expecteds = [assignment for assignment in assignments if assignment.targets[0].id == 'expect']
        return expecteds
    except:
        return None


CLIENT_TESTS_PATH = "~/work_projects/webapi/webapi/tests/test_client"


def get_files(path, banned_files=[]):
    return [f for f in os.listdir(path) if os.path.isfile(f)]
    
    
def get_filenames(path, banned_files=[]):
    file_list = []
    for root, _, files in os.walk(path):
        BANNED_FILES = banned_files
        for f in files:
            for string in BANNED_FILES:
                if string in f:
                    continue
            path = os.path.join(root, f)
            file_list.append(path)
    return file_list
    
filenames = get_filenames(CLIENT_TESTS_PATH, banned_files=['__init__.py', '.pyc', 'accounts.py, attributions.py'])

expect_string = """\
from decimal import Decimal

\"\"\"
to find an existing expectation, search by the name of the testclass or the test itself

please name all expectations like {classname}_{testname}_expectation
\"\"\"\n\n
"""


EXPECTATIONS_PATH = "~/names"

def get_file_ending(filename):
    return filename.replace('~/work_projects/webapi/webapi/tests/test_client/test_', '')
    
def get_expectations_path(filename):
    x = get_file_ending(filename)
    return os.path.join(EXPECTATIONS_PATH, x)

for f in filenames:
    expectations_path = get_expectations_path(f)
    print expectations_path
    write_content(expectations_path, expect_string)
    parsed = ast.parse(read_content(f))
    classes = parse_classes(parsed)
    for klass in classes:
        class_name = klass.name
        print class_name
        relevant_funcs = get_test_funcs_in_class(klass)
        for func in relevant_funcs:
            function_name =  func.name
            expected_name = "{klass}_{function}_expectation".format(klass=class_name, function=function_name)
            expecteds = get_expected_for_func(func)
            append_content(expectations_path, expected_name)
            append_content(expectations_path, '\n\n')
            # try:
            #     for e in expecteds:
            #         # import ipdb; ipdb.set_trace()
            #         the_dict = e.value
            #         this_dict = codegen.to_source(the_dict)
            #         content = expected_name + " = " + this_dict
            #         print content
            #         append_content(expectations_path, expected_name)
            #         append_content(expectations_path, '\n\n')
            # except TypeError:
            #     pass
                
        
    


"""
get all relevant files (skip for now)

get all classes from those files

get all functions from those files

within that class and function, find all assignments

find the assignments named "expected"

generate an expected assignment name ({class}_{function}_expected)


""" 

