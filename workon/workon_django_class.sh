#!/bin/bash

atom ~/django_class/eat_it
atom ~/django_class/lessons

google-chrome https://docs.djangoproject.com/en/1.10/intro/tutorial01/
google-chrome https://drive.google.com/drive/folders/0B215qw8y-MK8eUNobW03bnpDMzA
google-chrome /home/cchilders/django_class/lessons/0-setup_your_project.html
google-chrome /home/cchilders/django_class/lessons/1-containerizing_django_projects_and_managing_packages.html
google-chrome /home/cchilders/django_class/lessons/2-setting_up_your_database.html
google-chrome /home/cchilders/django_class/lessons/3-models.html
google-chrome /home/cchilders/django_class/lessons/4-admin.html
