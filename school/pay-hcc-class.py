#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re

from ez_scrip_lib import *

# anonymized now but when I needed it (when I attended HCC) this actually paid my tuition automatically using selenium webdriver
class PaymentCard(object):
    def __init__(self):
        self.number = "4444444444444"
        self.address1 = "My old address"
        self.address2 = ""
        self.state = "TX"
        self.city = "Austin"
        self.zipcode = "78741"
        self.expiration_month = "01"
        self.expiration_year = "18"
        self.security_code = "555"

card = PaymentCard()
hcc = HCCDriver()

hcc.login_webadvisor()
hcc.webadvisor_driver.access_link('Make a Payment')
hcc.webadvisor_driver.access_link('Pay on My Account')

soup = hcc.webadvisor_driver.soup
input_tag = soup.find_all('input', {"name": "LIST.VAR3_1"})[0]
debt = input_tag.attrs['value'].strip()
assert float(debt) > 0
hcc.webadvisor_driver.find_box_and_fill(value=debt, search_text="LIST.VAR1_1")
time.sleep(3)
hcc.webadvisor_driver.pick_select_box("VAR2", "VISA")
time.sleep(3)
hcc.webadvisor_driver.submit_form("SUBMIT2")
time.sleep(3)
hcc.webadvisor_driver.click_button("shortButton")
time.sleep(3)

def fill_all_fields(driver, fields):
    time.sleep(3)
    for key, value in fields.items():
        try:
            driver.find_box_and_fill(value=value, search_text=key)
        except:
            pass

fields = {
    'cc_number': card.number,
    'EXPMONTH': card.expiration_month,
    'EXPYEAR': card.expiration_year,
    'CVV2': card.security_code,
    'billingAddress1': card.address1,
    'billingAddress2': card.address2,
    'billingCity': card.city,
    'billingZip': card.zipcode,
    'billingAddress2': card.address2,
}

fill_all_fields(hcc.webadvisor_driver, fields)
time.sleep(3)
hcc.webadvisor_driver.pick_select_box("billingState", card.state)

time.sleep(3)
hcc.webadvisor_driver.click_button("btn_pay_cc")

# import ipdb; ipdb.set_trace()
