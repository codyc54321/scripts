#!/usr/bin/env python

import os, sys

HOMEPATH        = os.path.expanduser('~')
SCRIPTS_PATH    = os.path.join(HOMEPATH, "scripts")

def generate_path_variables():
    for root, dirs, files in os.walk(SCRIPTS_PATH):
        print(dirs)
        
def clean_alias(line):
    rgx = r"""alias\s*(?P<alias>\w+)[=]["'](?P<command>.*?)["']"""
    try:
        match   = re.search(rgx, line)
        alias   = match.group('alias')
        command = match.group('command')
        return alias + ":\n" + spaces(4) + command
    except:
        return ''

def clean_bash_function(line):
    rgx = r"""(?P<funcname>\w+)[(][)]\s*[{](?P<command>.*)[;]"""
    try:
        match       = re.search(rgx, line)
        funcname    = match.group('funcname')
        command     = match.group('command').strip()
        return funcname + ":\n" + spaces(4) + command
    except:
        return ''

def get_category_contents(category):
    try:
        category_contents_rgx   = r"(?<={}[*]\n).*?(?=[#] END)".format(category)
        match       = re.search(category_contents_rgx, bash_prof_content, flags=re.DOTALL)
        contents    = match.group()
        commands    = contents.split('\n')
        return commands
    except:
        return None