
import os, sys, re, time, subprocess
from subprocess import Popen, check_output, PIPE
from ez_scrip_lib.decorators import linux_or_mac


HOMEPATH          = os.path.expanduser('~')
PROJECT_PATH      = os.path.join(HOMEPATH, "projects")
WORK_PROJECT_PATH = os.path.join(HOMEPATH, "work_projects")
SCRIPTS_PATH      = os.path.join(HOMEPATH, "scripts")
SCRIPTS_TEMP_PATH = os.path.join(SCRIPTS_PATH, "tmp")
CWD               = os.getcwd()


#----------------------------------------------------------------------------------------------------

def create_file(the_file):
    with open(the_file, 'w') as f:
        f.write("")

#----------------------------------------------------------------------------------------------------

def read_lines(the_file):
    with open(the_file, 'r') as f:
        return f.readlines()

#------------------------------------------------------------------------------------------------------------------------

def read_content(the_file):
    with open(the_file, 'r') as f:
        return f.read()

#------------------------------------------------------------------------------------------------------------------------

def write_lines(the_file, lines):
    with open(the_file, 'w') as f:
        f.writelines(lines)

#------------------------------------------------------------------------------------------------------------------------

def write_content(the_file, content):
    with open(the_file, 'w') as f:
        f.write(content)

#----------------------------------------------------------------------------------------------------

def replace_content(the_file, old, new):
    with open(the_file, 'r') as f:
        content = f.read()
    content = content.replace(old, new)
    with open(the_file, 'w') as f:
        f.write(content)

#------------------------------------------------------------------------------------------------------------------------

def append_lines(the_file, lines):
    with open(the_file, 'a') as f:
        f.writelines(lines)

#------------------------------------------------------------------------------------------------------------------------

def append_content(the_file, content):
    try:
        current_content = read_content(the_file)
        with open(the_file, 'a') as f:
            f.write(content)
    except IOError:
        write_content(the_file, content)

#-------------------------------------------------------------------------------------------------------------------------

def clear_content(the_file):
    with open(the_file, 'w') as f:
        f.write('')

#-------------------------------------------------------------------------------------------------------------------------

def call_sp(command, *args, **kwargs):
    if args:
        command = command.format(*args)
    p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, **kwargs)
    output, err = p.communicate()
    print('\nOutput:\n')
    print(output.decode('utf-8'))
    print('\n\nErr:\n')
    print(err.decode('utf-8'))
    return output, err

#----------------------------------------------------------------------------------------------------

def join(*args):
    return os.path.join(*args)

#----------------------------------------------------------------------------------------------------

def get_filename(filepath):
    _, filename = os.path.split(filepath)
    return filename

#----------------------------------------------------------------------------------------------------

def get_filename_without_extension(filepath):
    _, filename = os.path.split(filepath)
    return os.path.splitext(filename)[0]

#----------------------------------------------------------------------------------------------------

def get_filenames(path, banned_files=[]):
    file_list = []
    for root, _, files in os.walk(path):
        for f in files:
            for string in banned_files:
                if string == f:
                    continue
            path = os.path.join(root, f)
            file_list.append(path)
        break
    return file_list

#----------------------------------------------------------------------------------------------------

def ensure_absolute_path(path, default_start_path):
    if not os.path.isabs(path):
        path = os.path.join(default_start_path, path)
    return path

#----------------------------------------------------------------------------------------------------

def ensure_home(path):
    if not path.startswith(HOMEPATH):
        path = os.path.join(HOMEPATH, path)
    return path

#----------------------------------------------------------------------------------------------------

def get_subdirectories(path):
    path            = ensure_home(path)
    shell_output    = check_output("ls -l | grep '^d'", **{'cwd': path, 'shell': True})
    return re.findall(r"(?<=\s)[a-zA-Z-_]+(?=\n)", shell_output)

#----------------------------------------------------------------------------------------------------

def ensure_dir_exists(path):
    if not path.startswith(HOMEPATH):
        path = os.path.join(HOMEPATH, path)
    try: os.makedirs(path)
    except: pass

#----------------------------------------------------------------------------------------------------

def get_output(command):
    return check_output(command, shell=True)

#----------------------------------------------------------------------------------------------------

def get_shellvar_path(var_name):
    return get_output('echo ${}'.format(var_name))

#----------------------------------------------------------------------------------------------------

def run(command):
    subprocess.call(command, shell=True)

#----------------------------------------------------------------------------------------------------

# @linux_or_mac({'command': {'linux': 'echo %s | sudo -S apt-get install -y {}' % os.environ['MYPW'], 'mac': 'pip install {}'}})
def install(program):
    run('echo {} | sudo -S apt-get install -y {}'.format(program))

#----------------------------------------------------------------------------------------------------

@linux_or_mac({'command': {'linux': 'sudo pip install {}', 'mac': 'pip install {}'}})
def pip_install(package, **kwargs):
    run(kwargs['command'].format(package))

#----------------------------------------------------------------------------------------------------

@linux_or_mac({'command': {'linux': 'sudo apt-get update', 'mac': 'brew update'}})
def update(**kwargs):
    call_sp(kwargs['command'])

#-------------------------------------------------------------------------------------------------------------------------

# def walklevel(some_dir, level=1):
#     some_dir = ensure_home(some_dir)
#     some_dir = some_dir.rstrip(os.path.sep)
#     assert os.path.isdir(some_dir)
#     num_sep = some_dir.count(os.path.sep)
#     for root, dirs, files in os.walk(some_dir):
#         yield root, dirs, files
#         num_sep_this = root.count(os.path.sep)
#         if num_sep + level <= num_sep_this:
#             del dirs[:]

#-------------------------------------------------------------------------------------------------------------------------

def clear_folder(dirpath):
    for root, dirs, files in os.walk(dirpath):
        for f in files:
            fullpath = os.path.join(root, f)
            clear_content(fullpath)

#-------------------------------------------------------------------------------------------------------------------------

def remove_files(starting_path, pattern_to_match):
    for root, dirs, files in os.walk(starting_path):
        for f in files:
            if re.match(pattern_to_match, f):
                fullpath = os.path.join(root, f)
                os.remove(fullpath)

#-------------------------------------------------------------------------------------------------------------------------

def get_project_path(project_name=None, relative_dir='projects'):
    """takes a project name, and defaults to $HOME/projects/project_name
       pass me a relative_dir if you want, so for $HOME/"""
    return ensure_home(os.path.join(relative_dir, project_name))

#-------------------------------------------------------------------------------------------------------------------------

def search_for_file(starting_path, pattern_to_match):
    for root, _, files in os.walk(starting_path):
        for f in files:
            if re.match(pattern_to_match, f):
                return os.path.join(root, f)
    return False

#-------------------------------------------------------------------------------------------------------------------------

def search_for_dir(dirname, path):
    for _, dirs, _ in os.walk(path):
        for this_dir in dirs:
            if dirname == this_dir:
                return True
    return False

#-------------------------------------------------------------------------------------------------------------------------

def get_current_dirname():
    _, current_dir = os.path.split(CWD)
    return current_dir
