
"""
from ast back to code: http://astunparse.readthedocs.org/en/latest/installation.html

ast: https://docs.python.org/2/library/ast.html
"""

import ast

try:
    import astunparse
except:
    astunparse = "you need to install astunparse to use my_ast.py"

from .sysadmin import read_content, read_lines, ensure_home

AST_OBJECTS = {'class': ast.ClassDef, 'function': ast.FunctionDef, 'assignment': ast.Assign,
               'if': ast.If, 'for': ast.For, 'module': ast.Module, 'name': ast.Name, 'isnot': ast.IsNot,
               'lambda': ast.Lambda, 'dict': ast.Dict, 'list': ast.List}


def get_ast_tree_for_file(filepath):
    """takes the full path like /home/username/scripts/updates.py, or a path relative to home like scripts/updates.py"""
    path = ensure_home(filepath)
    content = read_content(path)
    assert isinstance(content, str) == True
    tree = ast.parse(content)
    return tree


def parse_items(ast_object, desired):
    """ pass in a key from ez_scrip_lib.ast.AST_OBJECTS, like 'class' to get ast.ClassDef objects

    takes either an ast object or a list of ast objects"""

    try:
        return [item for item in ast.walk(ast_object) if isinstance(item, AST_OBJECTS[desired])]
    except:
        return [item for item in ast_object if isinstance(item, AST_OBJECTS[desired])]


def find_by_name(name, tree, type='class'):
    for item in tree.body:
        try:
            if item.name == name:
                if isinstance(item, AST_OBJECTS[type]):
                    return item
        except:
            pass
    return None


def get_class_names(tree):
    klasses = parse_items(tree, 'class')
    return [klass.name for klass in klasses]


def find_by_assignment_name(name, tree):
    for item in tree.body:
        if isinstance(item, AST_OBJECTS['assignment']):
            target = item.targets[0]
            if target.id == name:
                return item
    return None



class Visit(ast.NodeVisitor):

    @staticmethod
    def visit_ClassDef(node):
        print([n.value.id for n in node.bases])



class ClassASTHandler(object):

    def __init__(self, class_tree):
        self.tree = class_tree

    @property
    def name(self):
        return self.tree.name

    @property
    def base_classes(self):
        return [base.attr for base in self.tree.bases]

    @property
    def functions(self):
        return parse_items(self.tree, 'function')

    @property
    def function_names(self):
        return set(func.name for func in self.functions)

    def get_function_by_name(self, name):
        for func in self.functions:
            if func.name == name:
                return func
        return None


class FunctionASTHandler(object):

    def __init__(self, function_tree):
        self.tree = function_tree

    @property
    def name(self):
        return self.tree.name

    @property
    def args(self):
        return self.tree.args.args

    # handle var args, and kwargs
    @property
    def var_args(self):
        return self.tree.args.vararg

    @property
    def kwargs(self):
        return self.tree.args.kwarg

    @property
    def defaults(self):
        return self.tree.args.defaults

    @property
    def is_instance_method(self):
        return self.args[0].id == 'self'

    def get_assignment_by_name(self, name):
        body = self.tree.body
        assignments = parse_items(body, 'assignment')
        for item in assignments:
            handler = AssignmentASTHandler(item)
            if handler.name == name:
                return item


class AssignmentASTHandler(object):

    def __init__(self, assignment_tree):
        self.tree = assignment_tree

    @property
    def name(self):
        targets = self.tree.targets
        if len(targets) > 1:
            raise Exception("this assignment is unpacking a tuple or something, your AssignmentASTHandler can't handle that yet...")
        return self.tree.targets[0].id
