import re

from .decorators import single_item_or_list


#----------------------------------------------------------------------------------------------------

def raw_string(string):
    return string.encode('string-escape')

#-------------------------------------------------------------------------------------------------

def count_indentation(l):
    if l == '\n':
        return 0
    rgx = r"^\s+"
    match = re.match(rgx, l)
    if match:
        length = len(match.group())
        return length
    else:
        return 0

#----------------------------------------------------------------------------------------------------

def andify_list(the_list):
    def _string_clean(l):
        clean_list = []
        for item in l:
            clean_list.append(str(item))
        return clean_list

    the_list = _string_clean(the_list)
    if len(the_list) == 0:
        return ""
    if len(the_list) == 1:
        return the_list[0]
    if len(the_list) == 2:
        return the_list[0] + ' and ' + the_list[1]
    head = the_list[:-1]
    head_string = ", ".join(head)
    return head_string + " and " + the_list[-1]

#----------------------------------------------------------------------------------------------------

def list_to_string(mystery_item):
    if type(mystery_item) == str:
        return mystery_item
    string = ""
    for thing in mystery_item:
        if thing.endswith('\n'):
            string += thing
        else:
            string += thing + '\n'
    return string

#----------------------------------------------------------------------------------------------------

def choices_to_list(the_string):
    return re.findall(r"(\w+)", the_string)

#----------------------------------------------------------------------------------------------------

@single_item_or_list
def ensure_endswith(item, ender):
    """ensures an item or list ends with the second arg"""
    if item.endswith(ender):
        return item
    else:
        return item + ender

#----------------------------------------------------------------------------------------------------

@single_item_or_list
def ensure_startswith(item, starter):
    """ensures an item or list starts with the second arg"""
    if item.startswith(starter):
        return item
    else:
        return starter + item

#----------------------------------------------------------------------------------------------------

def is_commented(line):
    rgx = r"^\s*[#]"
    match = re.match(rgx, line)
    if match:
        return True
    else:
        return False

def spaces(howmany):
    return ' ' * howmany

#----------------------------------------------------------------------------------------------------

def extract_values(key=None, dictionaries=None):
    values = []
    for dct in dictionaries:
        try:
            values.append(dct[key])
        except KeyError:
            pass
    return values

#----------------------------------------------------------------------------------------------------

def replace_all(string, *args):
    for arg in args:
        string = string.replace(arg, "")
    return string

#----------------------------------------------------------------------------------------------------

def filter_iterable(filter_callback, iterable):
    generator = filter(filter_callback, iterable)
    return [item for item in generator]
