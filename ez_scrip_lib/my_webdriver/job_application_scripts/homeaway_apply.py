#!/usr/bin/env python

import os
import sys
import time

from ez_scrip_lib.sysadmin import read_content
from ez_scrip_lib.my_webdriver.general import FirefoxDriver
from ez_scrip_lib.my_webdriver.job_application_scripts.job_fields_data import RESUME_TEXT, HOMEAWAY_COVER_LETTER

RESUME_PATH = "/home/cchilders/Downloads/resume_cody_childers.docx.pdf"


try:
    URL = sys.argv[1]
except:
    URL = raw_input("Please enter the URL:\n")


driver = FirefoxDriver()
print("about to get url, %s" % URL)
driver.get(URL)
driver.wait('ImportCL', wait_time=5)

# if you give the argument from command line without quotes,
# bash interprest the & symbol as something else and you only get a partial URL
UNQUOTED_CLI_ARG_ERROR_MESSAGE = """Did you remember to quote the url that you passed as a CLI argument? Retry like:
    python %s "http://theurl" """ % __file__

if not driver.text_is_on_page("Upload Cover Letter"):
    raise Exception(UNQUOTED_CLI_ARG_ERROR_MESSAGE)


# fill basic input fields
INPUT_FIELDS_ACTIONS = [
    ("Cody", "jvfirstname"),
    ("Childers", "jvlastname"),
    ("5127365653", "jvfld-x-vV9Vfwe"),
    ("Austin", "jvfld-x-sV9Vfwb"),
    ("USA", "jvfld-x-uV9Vfwd"),
    ("TX", "jvfld-x-XV9VfwG"),
    ("78725", "jvfld-x-tV9Vfwc"),
    ("cchilder@mail.usf.edu", "jvemail"),
]


for action in INPUT_FIELDS_ACTIONS:
    driver.find_box_and_fill(*action)

driver.pick_select_box("jvfld-x3JrVfwS", "Bachelors/Undergraduate Degree")
driver.pick_select_box("jvfld-x1JrVfwQ", "English")
driver.pick_select_box("jvfld-x-yV9Vfwh", "1")
driver.pick_select_box("jvfld-x-zV9Vfwi", "16")
driver.pick_select_box("jvfld-x-yV9Vfwh", "1")


def paste_resume_in():
    add_resume_button = driver.find_element_by_xpath("//*[@id='jvApplyWithSection']/img[1]")
    add_resume_button.click()
    time.sleep(2)
    paste_resume_button = driver.locate_element("Paste your Resume")
    paste_resume_button.click()
    driver.wait("jvresume")
    driver.find_box_and_fill(RESUME_TEXT, "jvresume")

def generate_full_cover_letter_including_code():
    cover_letter = HOMEAWAY_COVER_LETTER
    cover_letter += __file__ + ":\n\n"
    cover_letter += read_content(os.path.realpath(__file__))
    return cover_letter

def paste_cover_letter_in():
    cover_letter = generate_full_cover_letter_including_code()
    driver.find_box_and_fill(cover_letter, "jvcoverletter")

paste_resume_in()
paste_cover_letter_in()

time.sleep(5)

send_application_button = driver.locate_element("Send Application")
send_application_button.click()

time.sleep(3)
driver.quit()
