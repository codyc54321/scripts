#!/usr/bin/env python

import os
import sys
import time

from ez_scrip_lib.sysadmin import read_content
from ez_scrip_lib.my_webdriver.general import FirefoxDriver
from ez_scrip_lib.my_webdriver.job_application_scripts.job_fields_data import LINKEDIN_URL, RESUME_TEXT, KHAN_COVER_LETTER

RESUME_PATH = "/home/cchilders/Downloads/resume_cody_childers.docx.pdf"


try:
    URL = sys.argv[1]
except:
    URL = raw_input("Please enter the URL:\n")

URL = URL.strip()


driver = FirefoxDriver()
print("about to get url, %s" % URL)
driver.get(URL)
driver.wait('first_name', wait_time=5)

# if you give the argument from command line without quotes,
# bash interprest the & symbol as something else and you only get a partial URL
UNQUOTED_CLI_ARG_ERROR_MESSAGE = """Did you remember to quote the url that you passed as a CLI argument? Retry like:
    python %s "http://theurl" """ % __file__

if not driver.text_is_on_page("Apply for this Job"):
    raise Exception(UNQUOTED_CLI_ARG_ERROR_MESSAGE)


# fill basic input fields
INPUT_FIELDS_ACTIONS = [
    ("Cody", "first_name"),
    ("Childers", "last_name"),
    ("5127365653", "phone"),
    # ("Austin", "jvfld-x-sV9Vfwb"),
    # ("USA", "jvfld-x-uV9Vfwd"),
    # ("TX", "jvfld-x-XV9VfwG"),
    ("cody.childers87", "job_application_answers_attributes_0_text_value"),
    ("cchilder@mail.usf.edu", "email"),
    (LINKEDIN_URL, "job_application_answers_attributes_0_text_value")
]


for action in INPUT_FIELDS_ACTIONS:
    driver.find_box_and_fill(*action)


driver.pick_select_box("job_application_gender", "1")
driver.pick_select_box("job_application_hispanic_ethnicity", "No")
time.sleep(0.2)
driver.pick_select_box("job_application_race", "5")
driver.pick_select_box("job_application_veteran_status", "1")
driver.pick_select_box("job_application_disability_status", "2")


def paste_resume_in():
    paste_resume_button = driver.find_element_by_xpath("//*[@id='main_fields']/div[8]/div/div[3]/a[4]")
    paste_resume_button.click()
    time.sleep(0.5)
    driver.find_box_and_fill(RESUME_TEXT, "resume_text")

def generate_full_cover_letter_including_code():
    cover_letter = KHAN_COVER_LETTER
    cover_letter += __file__ + ":\n\n"
    cover_letter += read_content(os.path.realpath(__file__))
    return cover_letter

def paste_cover_letter_in():
    cover_letter = generate_full_cover_letter_including_code()
    paste_cover_letter_button = driver.find_element_by_xpath("//*[@id='main_fields']/div[9]/div/div[3]/a[4]")
    paste_cover_letter_button.click()
    time.sleep(0.5)
    driver.find_box_and_fill(cover_letter, "cover_letter_text")


paste_resume_in()
paste_cover_letter_in()

# time.sleep(5)

# send_application_button = driver.locate_element("Send Application")
# send_application_button.click()
#
# time.sleep(3)
# driver.quit()
