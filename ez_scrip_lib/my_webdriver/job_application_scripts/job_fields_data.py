
LINKEDIN_URL = "https://www.linkedin.com/in/codychildersdjango/"

HOMEAWAY_COVER_LETTER = """
Hi, I was referred by Donna Dietrich, and I used to work with Richard Huettel who said he enjoys working at Homeaway.

I would be perfect for this role because I am very creative and hardworking, and I work hard to automate as much as I can.

For example, I applied for this job in an automated fashion (the main package used is https://github.com/codyc4321/webdriver_chauffeur).

If I'm the first candidate to apply for Homeaway jobs using Python scripts, I think I should get an interview :)

"""

KHAN_COVER_LETTER = """
Hi, I am a fan of Khan academy, and I used to use it in college.

I would be perfect for this role because I am very creative and hardworking, and I work hard to automate as much as I can.

For example, I applied for this job in an automated fashion (the main package used is https://github.com/codyc4321/webdriver_chauffeur).

If I'm the first candidate to apply for Khan Academy jobs automatically using Python scripts, I think I should get an interview :)

"""


RESUME_TEXT = """
Cody Childers
(512)736-5653
cchilder@mail.usf.edu


Summary:
Creator of the (world's first?) automated Django-app-writing Django app. Avid developer with over 2 amateur and 3 years professional web development experience. Intelligent and committed to excellence. Great analytical reasoning and problem solving mind. Great at quickly learning advanced scientific concepts. Devoting time and energy to expanding skills in programming and web development, with extensive experience in the web, sysadmin scripting, and automation.


Published Libraries:
A cool debugger decorator (pip install simple-debugger):
https://bitbucket.org/codyc54321/simple-debugger

A smooth text matching library (pip install intellimatch):
https://bitbucket.org/codyc54321/intellimatch

A convenience SQLAlchemy model for Flask (pip install flask-easy-model):
https://bitbucket.org/codyc54321/flask_easy_model

A fun convenience wrapper around Selenium Webdriver (pip install webdriver-chauffeur):
https://github.com/codyc4321/webdriver_chauffeur

My second ever program, a fully working (although rudimentary) command like poker program in Python. Please follow instructions in readme (pip install play-poker-demo):
https://bitbucket.org/codyc54321/poker_program_demo


Code:

A virtual assistant used in production (my desktop at home) that both emails and texts me a few times per day to remember to do productive things:
https://bitbucket.org/codyc54321/virtual_assistant

I automated tedious work of managing tasks in an issue tracker called Pivotal Tracker and opening pull requests on Github, to save 20-30 minutes of wasted time per day at Home Depot, saving thousands of dollars of time:
https://bitbucket.org/codyc54321/gitflow_automation


Technical Skills:
Python (scrapy, requests, lxml, PIL, Celery, RabbitMQ, South, Beautiful soup etc..)
Advanced regex skills (python)
Django framework (django-mptt, django rest FR, django messaging FR, etc..)
Flask, Docker
Postgres, MySQL, and neo4j databases
light PHP and Ruby (rails) training
Javascript, jquery, AJAX, knockout.js, React.js
CSS, Sass, Bootstrap
HTML 5
xml, xpath
Apache, nginx/gunicorn, and Heroku deployment
CSV/PDF rendering, including Prince
Extensive Python/Django/Flask/React development across Ubuntu, Fedora, and Windows


Education summary:
College-
B.A. Biochemistry
University of South Florida, Tampa, FL
3.3 GPA
Tech training like database admin, networking, etc



Employment History:

Home Depot
9/12/2016 - 9/31/2017
Fullstack developer (Python, Flask, Django, Reactjs, Javascript, knockoutjs, CSS, Bootstrap, Google Cloud)

Devops accomplishments
Responsible for deploying Flask apps after writing them, using Google Container Engine and Cloud SQL
Assisted in troubleshooting Jenkins files
Automated deployment of Flask apps requiring a dedicated server with SSH capabilities, using bash

General tasks/accomplishments
Filled in the lacking role of UX developer for my local team, using knockoutjs
Impressed management by rapidly implementing creative solutions without prompting, including creating an admin system from scratch where a tedious manual process was formerly required
Declared "very fast" by a respectable senior coworker


Trilogy Education Services
12/3/2016 - current
Coding bootcamp teaching assistant

General tasks/accomplishments
Quickly filled in as emergency replacement with minimal training, in a fast-paced class covering much curriculum
Provided expert technical guidance, troubleshooting difficult problems for languages I don't use, where other TA's weren't able to help
Became a go-to tutor for several students during project hours, being actively sought out for help building the most difficult projects

Authors.me
4/3/2016 - 7/12/2016
Part-time Fullstack developer (Flask, Backbonejs, Marionette, vanilla JS, CSS, HTML)

General tasks/accomplishments
Jumped into a large codebase head first, being productive on day 1
Quickly integrated  postmark mailer API to send ebooks to prospective publishers
Writing comprehensive unit tests in a codebase lacking tests
Impressed senior dev supervisor with thoroughness of note-taking on tickets, clarity of thought, and organization of git branches


Dimensional Fund Advisors
10/12/2015 - 9/1/2016
Python/Django developer

Devops accomplishments
Wrote an auto-install framework that is flexible to incorporate changes in dependencies/project requirements, and turned a 3 hour manual install any time a developer needs to set up a new machine into a 5 minute automated process, with fully functional interrelated projects installed correctly
Assisted with light Docker duties, including troubleshooting issues with the company's first Dockerfiles; completing the entire Docker official tutorial twice

General tasks/accomplishments

Learning cutting edge open source technologies like Docker, Fabric, Puppet, Python, and Django
Manage and update Dockerfile and Docker settings for my local machine
Have gone through the Docker tutorial twice
Writing unit tests with speed and accuracy that leave the VP of software engineering speechless
Building new and integrating existing API endpoints using Django rest framework
Automated entire python developer local deployment setup, turning a 3 hour process into 10 minutes anytime a new employee joins of a coworker wants to use a new machine
Solving abstruse problems with a poorly built legacy Perl codebase
Specializing in Python unittest coverage


Medi-Weightloss
3/9/2015 - 8/21/2015
Python/Django developer

Devops accomplishments

Specialized in scripting large-scale tasks for my supervisor, like gathering information on codebase, and automated setup of test features for a gigantic project of over 20 apps

General tasks/accomplishments
Using Python/Django, HTML, CSS, Sass, Javascript, SQL, and Postgresql, maintaining and adding software features that get a "good job" from upper management that almost never give out compliments
Built two medium sections of the internal EMR system and public facing website, adding a variable (0 to infinity) amount of buttons for categories, and calling a django REST framework API built by a coworker
Built the foundation of a large unittest framework, successfully faking 183 Django models, and writing unittests to check the accuracy of our data
Maintaining strict version control using Git, with an understanding of what tasks require branching and which can be done in dev
Automated a large portion of my job by writing a little code to write a huge amount of code, also writing scripts to do my work for me on one screen, as I continued working on the other screen
Started learning to implement an agile system for a small, flexible team


Florida Gulf Coast University
8/11/2014-3/6/2015
Full time student


US Student Loan Center
 2/13/2014-6/8/2014
Python/Django developer

Devops accomplishments

Installed and deployed my app across multiple servers

General tasks/accomplishments
Saved the company $250,000+ per year by finishing a solo automation project in Django well ahead of schedule, using Postgres, Python, Django, HTML, CSS, and XML, using several 3rd party APIs
Worked independently with minimal supervision in my first professional Django role
Saved ~2 hours per day * 44 sales reps with my automated e-contract program, also saving plenty of iffy deals from unsure customers (gaining copious love from sales reps)
Used version control through BitBucket, learned the Git command line, and learned Linux
Deployed my new app using Apache2 and mod_wsgi through Ubuntu



Eleison Pharmaceuticals
10/6/2013-12/10/2013

Pharmaceutical Chemist (contractor)

Produced 3000+ vials of a precious bone marrow cancer drug hands on in an extremely strict class 100 lab environment
Maintained perfect cleanliness/purity of all handled equipment, raw materials, and machinery
Was a key factor in producing more product than expected, ending the project early and saving a startup company a large amount of resources



Link-Systems International
9/14/2012-9/27/2013
QA/Algorithmic math programmer/web developer

Created interactive math problems in webpage form using an in-house interface, HTML, and an in-house TBern language
After being asked to learn the TBern language without the usual 2 week training program because the project is far behind schedule, I worked 55-60 hour weeks and finished in the top 4 in productivity
(45-69 problems per week) out of more than 60 co-workers for the vital 3 weeks before the deadline
Constantly taught new employees and other co-workers syntax and tricks in TBern to help the team
Created original feedback for solutions that are unique to each problem, learning to write creatively as an essential job skill
Helped troubleshoot coding problems with teammates, showing great patience and consideration for my team



References

Shashi Mannepali, Senior Engineering Manager, Home Depot
shashi_mannepalli@homedepot.com

Brett Payne, student success manager, Trilogy Education
512-618-8110
brett@trilogyed.com

Gary Wilson, lead Python developer, Dimensional Fund Advisors
gary.wilson@dimensional.com

Stefanie Berizka, expert software engineer, Medi-Weightloss
813-510-9226
stefanie.berizka@me.com

Salomon Crespin, Chemical Process Engineer, Link-Systems International
786-390-2409
salomoncrespin@gmail.com

Benjamin Andrews, instructional designer/corporate trainer, Medi-Weightloss
863-517-1604
bandrews@mediweightloss.com
"""
