import os

# usernames/emails
UFCU_USERNAME = os.environ.get("UFCU_USERNAME")
USFFCU_USERNAME = os.environ.get("USFFCU_USERNAME")
HCC_EMAIL = os.environ.get("HCC_EMAIL")

# pws
HCC_WEBADVISOR_PW = os.environ.get("HCC_WEBADVISOR_PW")
YOUTUBE_PW = os.environ.get("YOUTUBE_PW")
THE_USUAL = os.environ.get("THE_USUAL")
APE_NINE = os.environ.get("APE_NINE")
APE_NINE_SLASH = os.environ.get("APE_NINE_SLASH")
WORK_PW = os.environ.get("WORK_PW")
OOPS_PW = os.environ.get("OOPS_PW")
