#!/usr/bin/env python

import datetime, time, random

from ez_scrip_lib.crugs_list.ad_drivers.data_room_rentals import *
from ez_scrip_lib.crugs_list.ad_drivers.ad_driver_room_rentals import RoomRentalPostsDriver


DATA = {
    'regular': {
        'PostingTitle': regular_room_titles,
        'PostingBody': regular_room_body,
        'Ask': ASKING_PRICE,
        'available_on': datetime.date(2017, 11, 1),
        'images': upstairs_room_behind_desk_images
    },
    'live_in_help': {
        'PostingTitle': livein_maid_titles,
        'PostingBody': livein_maid_body,
        'Ask': ASKING_PRICE,
        'available_on': datetime.date(2017, 11, 1),
        'images': upstairs_room_behind_desk_images
    }
}


def run_room_posting(category, data={}, driver=None, quit=False, city="austin"):
    if not driver:
        driver = RoomRentalPostsDriver()
    driver.post_room_rental(
        category=category,
        city=city,
        **data
    )
    time.sleep(4)
    if quit:
        driver.driver.quit()


def run_all_postings(data, categories, delay=30):
    choice = raw_input('Please pick:\n(1) regular ad\n(2) live in help ad\n')
    posting_type_choices = ['regular', 'live_in_help']
    # since list indexes start at 0, subtract 1 to get the choice back to 0 or 1
    posting_type = DATA[posting_type_choices[int(choice) - 1]]
    driver = RoomRentalPostsDriver()
    driver.delete_all_active_ads()
    COUNT_RAN = 0
    for category in categories:
        COUNT_RAN += 1
        if COUNT_RAN >= len(categories):
            run_room_posting(category, data=data[posting_type], driver=driver, quit=True)
        else:
            print('Sleeping for {} seconds...'.format(delay))
            time.sleep(delay)
            run_room_posting(category, data=data[posting_type], driver=driver)



# run_room_posting("rooms_and_shares", data=data[TYPE], driver=driver)
# run_room_posting("sublets_and_temp", data=data[TYPE], driver=driver)

run_all_postings(data=DATA, categories=['rooms_and_shares', 'sublets_and_temp'])
