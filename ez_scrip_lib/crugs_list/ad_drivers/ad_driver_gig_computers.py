import datetime
import os
import time

from faker import Faker
FAKER = Faker()

from ez_scrip_lib.crugs_list import CraigslistDriver


BODY_TEXT = """
Read entire ad and follow instructions to be considered

Intern needed to finish small app, 5-6 weeks left

Work from home

Learn Python and Javascript

No experience necessary, classes taken or experience cool but not required

Get taught how to program for free and use your code and the final product in your portfolio to get your first full-time job

This is primarily a training internship, with expert guidance as you build this system. I will personally train you, act as a reference if you perform, and if you do great, I will send a lump sum upon completion

*RESPONSE FILTERING IS AUTOMATED, please do not waste your own time with insincere response*

Responses that prove one didn't read the ad are automatically deleted. To be hired you must:

Start your response with the word "python" in all caps

You must next include space(s) after PYTHON and days you can work, 1 or 2 letters, from this list:
[su, m, tu, w, th, f, sa]

An example is: "mtusa", which gets sent to me as "monday, tuesday, saturday"

Response MUST include phone number (in any format) to be considered. Serious inquiries only!!!!

Do not ask questions about the task, it is programming from home in javascript or python, whichever you prefer. It is short-term and needs to be finished quickly. I can work around your schedule; it's a work from home job. An example of an automatically accepted (for trial period) response would be:

"PYTHON mfsasu Hi, I'm {your_name}, I'd love to finish this app. I can work nights m and f, and all day saturday and sunday. {your_phone_number} nice to meet you, can't wait" <-- automatically accepted

"Hey bro, what's up?! I'm {name}! I love apps. Where's it located?" <-- this email gets deleted, automatically rejected

This email is not automatically posted, but the response we get are automatically screened. Don't flag me bro.

As this is automated I literally cannot discriminate. First person to follow the listed instructions wins

All candidates who follow the instructions will receive response. After your phone call so you know we're real, please remember to send your email. You will receive the CTO or CEO's email after your screen.

This is not your typical inconsiderate, rude corporate environment, this is a distributed, worldwde, online startup company offering free training...executives will NOT ignore you.

If you can read and follow instructions, congrats, you're about the be on the cutting edge of tech. We automate everything that should be automated, and refuse to automate everything that shouldn't be automated.

Bonuses can be paid upon milestones and are negotiable

Thank you
"""



class FakeGigPostObject(object):
    def __init__(self):
        self.contact_name = ""
        self.phone_number = ""
        self.specific_location = "Austin"
        self.title = FAKER.random_element(possible_titles)
        self.street = ""
        self.cross_street = ""
        self.state = ""
        self.city = ""
        self.postal_code = "78741"
        self.body = BODY
        self.by_phone = False
        self.private_room = True
        self.private_bath = False
        self.laundry = "1"
        self.rent = "550"
        self.available_on = datetime.date(2017, 2, 3)
        self.parking = "5"
        self.cats_ok = True
        self.dogs_ok = True
        self.furnished = True
        self.no_smoking = False
        self.wheelchair_accessible = True
        self.square_feet = "800"

TASKS = [
    (posting_obj.square_feet, 'Sqft'),
    (posting_obj.rent, 'Ask'),
    (posting_obj.available_on.month, 'moveinMonth'),
    (posting_obj.available_on.day, 'moveinDay'),
    (posting_obj.available_on.year, 'moveinYear'),
    (posting_obj.private_room, 'private_room'),
    (posting_obj.private_bath, 'private_bath'),
    (posting_obj.laundry, 'laundry'),
    (posting_obj.parking, 'parking'),
    (posting_obj.furnished, 'is_furnished'),
    # (posting_obj.no_smoking, 'no_smoking'),
    # (posting_obj.wheelchair_accessible, 'wheelchaccess'),
]


class ComputerGigPostsDriver(CraigslistDriver):

    def post_room_rental(self, posting_obj, category='gig_computers', images=[]):
        self.images = images
        self.post_an_add(posting_obj, category)
        for task in TASKS:
            self.driver.complete_field(*task)
        self.driver.click_button('go')
        time.sleep(5)
        self.driver.click_button('continue')
        self.driver.basic_sleep('short')

        self.driver.basic_sleep('long')
        element = self.driver.locate_element("publish")
        element.click()
        self.driver.basic_sleep('long')
        self.ensure_recent_ad_is_published()
