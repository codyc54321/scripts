
titles = [
    "*** Professional Wedding Photography Services ***",
    "Andres Photography Services Local/Professional/Experienced",
    "Bridal/Wedding/Engagement Photography Services - Professional",
]

body = """
At Andres Photography, the following professional photography services are available to be preformed:

On Location OR Studio Photography Available for:

* Engagement and Couples Portraits
* Bridal Portrait Sessions
* Pre-Wedding Photography
* Ceremonial and Reception Photography
(Videography services are also available)

Andres Photography is exceptional at capturing your timeless and priceless memories on camera! The dedication to your artistic vision is completely achievable. It is our highest priority to exceed your expectations! Passion and drive help to captivate and amaze with stunning and beautiful imagery.
Through the lens of hi-resolution, state-of-the-art cameras, your memories are preserved and captured. Precise timing, mastered understanding of lighting, conceptual composition and keen, vivid imagination, will help the idea you've envisioned, come to life!

**Each and every wedding is different and everyone's budgets are unique; it is because of these factors, a personalized package is catered around your beautiful event's specific needs and cost.**

Here is an idea of what is available in your wedding's professional photography package:

* Expert direction
* Digital editing i.e. red-eye removal, light adjustments
* Digitally enhanced/specially filtered images (photographer's creative vision)
* Rights and Access to ALL edited images (uploaded to DropBox for easy downloading and sharing)
* FlashDrives containing ALL of the unedited images
* Canvas Prints (Gallery Wrapped) ((ALL SIZES))
* Traditional Prints available
* 30-40 Page Personally designed, hardcover, printed photobooks (designed and printed)
* Professional Videography Services available
* Hair and Makeup Services can also be included or scheduled

Call TODAY to book your next session with Andres Photography... together we'll capture some amazing photos that you and your family will treasure for a lifetime!!

Website: http://www.photographyinsanantoniotx.com

Facebook: https://www.facebook.com/AndresPhotography.SA

Thumbtack: https://www.thumbtack.com/tx/san-antonio/creative-photographers
"""
