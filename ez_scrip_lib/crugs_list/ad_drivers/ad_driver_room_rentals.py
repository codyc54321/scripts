import os
import datetime
import time

from faker import Faker
FAKER = Faker()

from ez_scrip_lib.config import THE_USUAL
from ez_scrip_lib.crugs_list import CraigslistDriver


class FakeRoomRentalPostObject(object):
    def __init__(self):
        self.contact_name = ""
        self.contact_phone = ""
        self.specific_location = "Austin"
        # self.PostingTitle = FAKER.random_element(possible_titles)
        self.xstreet0 = "Cottingham Ln"
        self.xstreet1 = "Austins Colony Bldv"
        self.state = "TX"
        self.city = "Austin"
        self.postal_code = "78725"
        # self.PostingBody = BODY
        self.contact_phone_ok = False
        self.contect_text_ok = False
        self.private_room = True
        self.private_bath = False
        self.laundry = "1"
        # asking price
        # self.Ask = "550"
        # self.available_on = datetime.date.today()
        self.parking = "5"
        self.pets_cat = False
        self.pets_dog = False
        self.is_furnished = True
        self.no_smoking = False
        self.wheelchair_accessible = True
        self.Sqft = ""


class RoomRentalPostsDriver(CraigslistDriver):

    def __init__(self, username="cchilder@mail.usf.edu", password=THE_USUAL):
        super(RoomRentalPostsDriver, self).__init__(username=username, password=password)

    def post_room_rental(self, category='rooms_and_shares', images=[], city='austin',
            available_on=datetime.date.today, **kwargs):
        """ requires PostingTitle (as a list of titles), PostingBody, and Ask """
        # TODO: set city to 'austin' and substitute the city in the kwargs to be used in posting_obj
        self.city = city
        self.start_url = self.generate_start_url_by_city()

        self.images = images

        kwargs.update({'specific_location': city.capitalize(), 'city': city.capitalize()})
        posting_obj = self.generate_posting_obj(kwargs)

        self.post_an_add(category, posting_obj)
        self.fill_form_fields(self.get_form_tasks(available_on))
        self.driver.click_button('go')
        # self.driver.wait(search_item="imgbox", search_type="class")
        time.sleep(5)
        self.driver.click_button('continue')
        self.driver.basic_sleep('short')
        self.add_images_and_move_on()
        self.driver.basic_sleep('long')
        element = self.driver.locate_element("publish")
        element.click()
        self.driver.basic_sleep('long')
        self.ensure_recent_ad_is_published()

    def generate_posting_obj(self, attrs):
        posting_obj = FakeRoomRentalPostObject()
        for key, value in attrs.items():
            if key == 'PostingTitle':
                setattr(posting_obj, 'PostingTitle', FAKER.random_element(value))
            else:
                setattr(posting_obj, key, value)
        if not 'available_on' in attrs.keys():
            setattr(posting_obj, 'available_on', datetime.date.today())
        return posting_obj

    def get_form_tasks(self, available_on):
        return [
            'Sqft',
            'Ask',
            (available_on.month, 'moveinMonth'),
            (available_on.day, 'moveinDay'),
            (available_on.year, 'moveinYear'),
            'private_room',
            'private_bath',
            'laundry',
            'parking',
            'is_furnished',
        ]
