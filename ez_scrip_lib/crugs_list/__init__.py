import os
import datetime, time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from ez_scrip_lib.my_webdriver.general import FirefoxDriver

ACCOUNT_HOME = "https://accounts.craigslist.org/login/home"


class FakeCraigslistObject(object):
    def __init__(self):
        self.contact_name = ""
        self.phone_number = ""
        self.specific_location = "Austin"
        self.title = "best room you've ever seen"
        self.street = "Kramer Ln"
        self.cross_street = ""
        self.state = "TX"
        self.city = "Austin"
        self.postal_code = "78758"
        self.body = "Got nice room open, hit me up"
        self.by_phone = False
        self.private_room = True
        self.private_bath = True
        self.laundry = "1"
        self.rent = "600"
        self.available_on = datetime.date.today()
        self.parking = "5"
        self.cats_ok = True
        self.dogs_ok = True
        self.furnished = True
        self.no_smoking = False
        self.wheelchair_accessible = True
        self.square_feet = "800"


class CraigslistDriver(object):

    def __init__(self, username="cchilder@mail.usf.edu", password=os.environ.get("THE_USUAL"),
            city="austin"):
        super(CraigslistDriver, self).__init__()
        self.username  = username
        self.password  = password
        self.set_city(city)
        self.driver = FirefoxDriver()
        assert self.username != None and self.password != None

    CATEGORY_DATA = {
        # ROOM RENTALS
        'rooms_and_shares': ('ho', '18'),
        'sublets_and_temp': ('ho', '39'),
        'gig_computers': ('go', 'G', '110'),
        'creative_services': ('so', '77'),
        'event_services': ('so', '79'),

    }

    CITY_TO_ZIPCODE_MAP = {
        # texas
        'dallas': 75235,
        'austin': 78701,
        # 'houston': 77009,
        'waco': 76707,
        'sanantonio': 78216,
        'killeen': 76542,

        # northwest
        'seattle': 98101,

        # southwest
        'phoenix': 85013,

        # southeast
        'raleigh': 27513,
        'tampa': 33605,
        'miami': 33128,

        # northeast
        'newyork': 10016,

        # cali
        'sfbay': 94110
    }

    def post_an_add(self, category_name, posting_obj):
        self.driver.basic_sleep()
        # import ipdb; ipdb.set_trace()
        self.ensure_logged_in()
        self.driver.basic_sleep()
        self.driver.get(self.start_url)
        self.driver.basic_sleep()
        self.go_to_posting_page()
        self.driver.basic_sleep('short')
        self.pick_posting_category(category_name)
        self.fill_form_fields(posting_obj)
        self.driver.basic_sleep()

    # def fill_main_info(self, posting_obj):
    #     time.sleep(3)
    #     self.fill_form_fields(MAIN_TASKS, posting_obj)

    def fill_form_fields(self, posting_obj):
        attrs = dir(posting_obj)
        real_attrs = [attr for attr in attrs if not attr.startswith('__')]

        for attr in real_attrs:
            # if attr == 'available_on':
            #     continue
            try:
                if isinstance(attr, str):
                    self.driver.complete_field(getattr(posting_obj, attr), attr)
                else:
                    self.driver.complete_field(*attr)
            except:
                print("Failed to fill form field with type '%s'" % attr)

    def pick_posting_category(self, category_name):
        radio_buttons = self.CATEGORY_DATA[category_name]
        for button in radio_buttons:
            self.driver.basic_sleep()
            try:
                self.driver.pick_radio_button(button)
            except:
                pass
            time.sleep(3)
        while True:
            PICK_LOCATION = self.driver.text_is_on_page("choose the location that fits best")
            if PICK_LOCATION and self.driver.text_is_on_page("alamo square"):
                self.driver.pick_radio_button("149")
            elif PICK_LOCATION and self.driver.text_is_on_page("East Harlem"):
                self.driver.pick_radio_button("121")
            elif PICK_LOCATION:
                self.driver.pick_radio_button("1")
            else:
                break
            self.driver.basic_sleep()

    def go_to_posting_page(self):
        # TODO: this is bad and should be turned back on...having problems with nyble recruitment email
        # self.ensure_logged_in()
        self.driver.get(self.start_url)
        self.driver.basic_sleep('short')
        self.driver.click_button('my account')
        self.driver.basic_sleep('long')
        form = self.driver.find_element_by_class_name("new_posting_thing")
        form.submit()
        self.driver.basic_sleep('short')

    def add_images_and_move_on(self):
        self.driver.basic_sleep('short')
        if hasattr(self, 'images'):
            for filepath in self.images:
                self.add_photo(filepath)
                self.driver.basic_sleep('long')
        element = self.driver.locate_element("done with images")
        element.click()

    def delete_all_active_ads(self):
        self.login()
        while True:
            try:
                self.delete_generic_ad()
            except:
                break

    def delete_generic_ad(self):
        """ assumes youre on the main account page where all your ads are listed """
        self.driver.click_button("delete")
        self.driver.basic_sleep()
        self.driver.back()
        self.driver.basic_sleep()

    def login(self):
        self.driver.basic_sleep()
        self.driver.get(self.start_url)
        self.driver.basic_sleep()
        self.driver.access_link(search_text="my account")
        self.driver.basic_sleep()
        self.driver.find_box_and_fill_as_person(search_text="inputEmailHandle", value=self.username)
        self.driver.find_box_and_fill_as_person(search_text="inputPassword", value=self.password)
        self.driver.submit_form(search_text="Log in")
        self.driver.basic_sleep('long')

    def ensure_logged_in(self):
        if not self.logged_in:
            self.login()

    @property
    def logged_in(self):
        try:
            return self.driver.text_is_on_page(self.username)
        except:
            pass

    def fill_and_wait(self, input_text=None, search_text=None):
        self.driver.find_box_and_fill(input_text, search_text=search_text)
        self.driver.basic_sleep('short')

    def add_photo(self, filepath_to_photo):
        photo_filepath_input_box = self.driver.find_element_by_xpath("//input[@type='file']")
        try:
            photo_filepath_input_box.send_keys(filepath_to_photo)
        except:
            print("Failed to add photo {0}".format(filepath_to_photo))

    def ensure_recent_ad_is_published(self):
        self.driver.get(ACCOUNT_HOME)
        # for some reason, the ad doesnt show up initially
        self.driver.basic_sleep()
        self.driver.refresh()

        all_a_tags = self.driver.soup.find_all("a")
        # base_url looks like https://austin.craigslist.org/
        base_url = self.start_url + "/" if not self.start_url.endswith("/") else self.start_url
        if 'https' in base_url:
            other_url = base_url.replace('https', 'http')
        else:
            other_url = base_url.replace('http', 'https')
        urls = [a_tag.attrs["href"] for a_tag in all_a_tags if base_url in a_tag.attrs["href"] \
                    or other_url in a_tag.attrs["href"] \
                    and not a_tag.attrs["href"] == base_url or a_tag.attrs["href"] == base_url]

        self.driver.get(urls[0])
        element = self.driver.locate_element("publish")
        if element:
            try:
                print("\n\nThis ad didn't publish...publishing now\n\n")
                self.driver.basic_sleep('short')
                element.click()
            except:
                pass

    def set_city(self, city):
        self.city = city
        self.start_url = self.generate_start_url_by_city(self.city)

    def generate_start_url_by_city(self, city=None):
        if not city:
            city = self.city
        lowercase_city = city.lower()
        return "https://{city}.craigslist.org".format(city=city)



# class CraigslistDriver(object):
#
#     def __init__(self,  username="cchilder@mail.usf.edu", password="childers9",
#             start_url="https://austin.craigslist.org"):
#         super(CraigslistDriver, self).__init__()
#         self.username  = username
#         self.password  = password
#         self.start_url = start_url
#         self.home_url = "https://accounts.craigslist.org/login/home"
#
#     """
#     General:
#
#     ho  = housing offered
#     so  = service offered
#     fso = for sale by owner
#
#     For rentals:
#
#     18 = rooms & shares
#     39 = sublets & temporary
#     1  = apts/housing for rent
#     """
#
#     CATEGORY_DATA = {
#         # ROOM RENTALS
#         'rooms_and_shares': ('ho', '18'),
#         'sublets_and_temp': ('ho', '39'),
#
#     }
#
#     def post_room_rental(self, posting_obj):
#         self.post_an_add(posting_obj, 'rooms_and_shares')
#         TASKS = [
#             (posting_obj.square_feet, 'Sqft'),
#             (posting_obj.rent, 'Ask'),
#             (posting_obj.available_on.month, 'moveinMonth'),
#             (posting_obj.available_on.day, 'moveinDay'),
#             (posting_obj.available_on.year, 'moveinYear'),
#             (posting_obj.private_room, 'private_room'),
#             (posting_obj.private_bath, 'private_bath'),
#             (posting_obj.laundry, 'laundry'),
#             (posting_obj.parking, 'parking'),
#             (posting_obj.furnished, 'is_furnished'),
#             # (posting_obj.no_smoking, 'no_smoking'),
#             # (posting_obj.wheelchair_accessible, 'wheelchaccess'),
#         ]
#         for task in TASKS:
#             self.driver.complete_field(*task)
#         self.driver.click_anything('go')
#         self.driver.basic_sleep('short')
#         self.driver.click_anything('continue')
#         self.driver.basic_sleep('short')
#         self.add_photo('/Users/cchilders/pictures/smileyguy.jpeg')
#         self.driver.basic_sleep('long')
#         element = self.driver.locate_element("done with images")
#         element.click()
#         self.driver.basic_sleep('long')
#         element = self.driver.locate_element("publish")
#         element.click()
#         self.driver.basic_sleep('long')
#         self.ensure_recent_ad_is_published()
#
#     def ensure_recent_ad_is_published(self):
#         self.driver.get(self.home_url)
#         links = self.driver.get_elements_by_xpath("//a")
#         # base_url looks like https://austin.craigslist.org/
#         base_url = self.start_url + "/" if not self.start_url.endswith("/") else self.start_url
#         urls = [link.get_attribute("href") for link in links if base_url in link.get_attribute("href") \
#                 and not link.get_attribute("href") == base_url]
#         self.driver.get(urls[0])
#         element = self.driver.locate_element("publish")
#         if element:
#             try:
#                 element.click()
#             except:
#                 pass
#
#     def post_an_add(self, posting_obj, category_name):
#         if not self.logged_in:
#             self.login()
#         self.driver.basic_sleep()
#         time.sleep(13)
#         self.go_to_posting_page()
#         self.driver.basic_sleep('short')
#         self.pick_posting_category(category_name)
#         self.fill_main_info(posting_obj)
#         self.driver.basic_sleep()
#
#     def fill_main_info(self, posting_obj):
#         TASKS = [
#             (posting_obj.phone_number, 'contact_phone'),
#             (posting_obj.contact_name, 'contact_name'),
#             (posting_obj.title, 'PostingTitle'),
#             (posting_obj.postal_code, 'postal_code'),
#             (posting_obj.body, 'PostingBody'),
#             (posting_obj.street, 'xstreet0'),
#             (posting_obj.cross_street, 'xstreet1'),
#             (posting_obj.city, 'city'),
#         ]
#         time.sleep(3)
#         for task in TASKS:
#             self.driver.complete_field(*task)
#
#     def fill_form_fields(self, posting_obj):
#         attrs = dir(posting_obj)
#         real_attrs = [attr for attr in attrs if not attr.startswith('__')]
#
#         for attr in real_attrs:
#             # if attr == 'available_on':
#             #     continue
#             try:
#                 if isinstance(attr, str):
#                     self.driver.complete_field(getattr(posting_obj, attr), attr)
#                 else:
#                     self.driver.complete_field(*attr)
#             except:
#                 print("Failed to fill form field with type '%s'" % attr)
#
#     # def pick_posting_category(self, category_name):
#     #     radio_buttons = self.CATEGORY_DATA[category_name]
#     #     for button in radio_buttons:
#     #         print("button: ", button)
#     #         try:
#     #             self.driver.pick_radio_button(button)
#     #         except:
#     #             import ipdb; ipdb.set_trace()
#     #         self.driver.basic_sleep('short')
#
#     def pick_posting_category(self, category_name):
#             radio_buttons = self.CATEGORY_DATA[category_name]
#             for button in radio_buttons:
#                 self.driver.basic_sleep()
#                 try:
#                     self.driver.pick_radio_button(button)
#                 except:
#                     pass
#                 time.sleep(3)
#             while True:
#                 PICK_LOCATION = self.driver.text_is_on_page("choose the location that fits best")
#                 if PICK_LOCATION and self.driver.text_is_on_page("alamo square"):
#                     self.driver.pick_radio_button("149")
#                 elif PICK_LOCATION and self.driver.text_is_on_page("East Harlem"):
#                     self.driver.pick_radio_button("121")
#                 elif PICK_LOCATION:
#                     self.driver.pick_radio_button("1")
#                 else:
#                     break
#                 self.driver.basic_sleep()
#
#     def go_to_posting_page(self):
#         form = self.driver.find_element_by_class_name("new_posting_thing")
#         form.submit()
#         self.driver.basic_sleep('short')
#
#     def login(self):
#         self.driver = FirefoxDriver()
#         self.driver.basic_sleep()
#         self.driver.get(self.start_url)
#         self.driver.basic_sleep()
#         self.driver.access_link(search_text="my account")
#         self.driver.basic_sleep()
#         self.driver.find_box_and_fill_as_person(search_text="inputEmailHandle", value=self.username)
#         self.driver.find_box_and_fill_as_person(search_text="inputPassword", value=self.password)
#         self.driver.submit_form(search_text="Log in")
#
#     @property
#     def logged_in(self):
#         try:
#             return self.username in self.driver.soup
#         except:
#             pass
#
#     def fill_and_wait(self, input_text=None, search_text=None):
#         self.driver.find_box_and_fill(input_text, search_text=search_text)
#         self.driver.basic_sleep('short')
#
#     def add_photo(self, filepath_to_photo):
#         photo_filepath_input_box = self.driver.find_element_by_xpath("//input[@type='file']")
#         try:
#             photo_filepath_input_box.send_keys(filepath_to_photo)
#         except:
#             print("Failed to add photo {0}".format(filepath_to_photo))
