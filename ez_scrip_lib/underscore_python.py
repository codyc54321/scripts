

def pluck(these_lists, property_name):
    items = []
    for this_dict in these_lists:
        try:
            items.append(this_dict[property_name])
        except KeyError:
            pass
    return items