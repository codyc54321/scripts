import os, filecmp, difflib

from .sysadmin import *
from .my_diff import content_the_same


def get_full_repopath_and_realpath(repo_path=None, real_path=None):
    """repo_path is relative to ~/scripts; real_path is the path relative to HOME"""
    full_repo_path    = os.path.join(SCRIPTS_PATH, repo_path)
    full_real_path    = os.path.join(HOMEPATH, real_path)
    return full_repo_path, full_real_path


def initialize_file_in_scripts_project(source_path, relative_repo_path, repo_path):
    # make a scripts repo copy of the file and push if need be
    source_path = ensure_home(source_path)
    repo_path = ensure_home(repo_path)
    # initialize d down here so repo_path can be set as a full path by ensure_home()
    d = {'cwd': repo_path}
    full_repo_path = os.path.join(repo_path, relative_repo_path)
    write_content(full_repo_path, read_content(source_path))
    # uses relative repo_path since it's adding to the current scripts repo given in 'd' dictionary
    add_command = 'git add {}'.format(relative_repo_path)
    call_sp(add_command, **d)
    commit_command = 'git commit {} -m "{} autoupdate, initial commit of this file"'.format(full_repo_path, source_path)
    call_sp(commit_command, **d)
    call_sp('git push', **d)
    
    
def pull_system_file_from_scripts_project_and_update_it(real_path=None, repo_path=None):
    """repo_path is relative to ~/scripts; real_path is the path relative to HOME"""
    FULL_REPO_PATH, FULL_REAL_PATH = get_full_repopath_and_realpath(repo_path, real_path)
    d = {'cwd': SCRIPTS_PATH}
    if os.path.exists(FULL_REPO_PATH):
        call_sp('git pull', **d)
        content_after_pull = read_content(FULL_REPO_PATH)
        write_content(FULL_REAL_PATH, content_after_pull)
    else:
        initialize_file_in_scripts_project(FULL_REAL_PATH, repo_path, SCRIPTS_PATH)


def push_system_file_to_scripts_project(real_path=None, repo_path=None):
    FULL_REPO_PATH, FULL_REAL_PATH = get_full_repopath_and_realpath(repo_path, real_path)
    
    d = {'cwd': SCRIPTS_PATH}
    if os.path.exists(FULL_REPO_PATH):
        # filecmp.cmp returns True if 2 files are the same
        if not filecmp.cmp(FULL_REAL_PATH, FULL_REPO_PATH):
            new_written_content = read_content(FULL_REAL_PATH)
            write_content(FULL_REPO_PATH, new_written_content)
            call_sp('git commit {} -m "{} autoupdate"'.format(FULL_REPO_PATH, FULL_REAL_PATH), **d)
            call_sp('git push', **d)
    else:
        initialize_file_in_scripts_project(FULL_REPO_PATH, FULL_REAL_PATH)
