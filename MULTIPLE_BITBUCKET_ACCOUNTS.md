http://stackoverflow.com/questions/8186441/multiple-bitbucket-accounts

set up ssh for different accounts


.ssh/config::

    Host old
      HostName bitbucket.org
      IdentityFile ~/.ssh/old
    Host tutorial_account
      HostName bitbucket.org
      IdentityFile ~/.ssh/tutorial_account

now you can call::

    git remote add origin git@tutorial_account:codyc54321_tutorial/project_name.git

    git remote add origin git@host:username/project.git
