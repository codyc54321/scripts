#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re

from ez_scrip_lib import *

bucket_hoss = BitbucketAPIHoss()

all_project_names = bucket_hoss.get_bitbucket_project_names()

ensure_dir_exists(PROJECT_PATH)

os.chdir(PROJECT_PATH)

TOP_LEVEL_PROJECTS = ['scripts', 'my_documents']

for project in all_project_names:
    if project not in TOP_LEVEL_PROJECTS:
        clone_project(project=project, dir_to_clone_in=PROJECT_PATH)

os.chdir(HOMEPATH)

for project in TOP_LEVEL_PROJECTS:
    clone_project(project=project)
