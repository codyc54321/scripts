#!/usr/bin/env python

import subprocess
import os
import sys
import optparse
import time

from ez_scrip_lib import *

# http://stackoverflow.com/questions/13788485/create-new-repo-on-bitbucket-from-git-bash-terminal

d = {'cwd': CWD}

def get_current_dirname():
    _, current_dir = os.path.split(CWD)
    return current_dir

project_path = CWD

try:
    print(sys.argv)
    project_name = sys.argv[1]
except IndexError:
    project_name = get_current_dirname()
print(project_name)

hoss = BitbucketAPIHoss()
hoss.create_repository(name=project_name)
d['cwd'] = project_path
time.sleep(1)
call_sp("curl --user codyc54321:{} https://api.bitbucket.org/1.0/repositories/ \
        --data name={} --data is_private='true'".format(APE_NINE, project_name))
call_sp('git init', **d)
git_string = 'git remote add origin git@bitbucket.org:codyc54321/{}.git'.format(project_name, d['cwd'])
print(("\n{}\n\n".format(git_string)))
call_sp(git_string)
time.sleep(1)

gitignore_path = os.path.join(project_path, '.gitignore')
if not os.path.exists(gitignore_path):
    call_sp('touch .gitignore', **d)
    time.sleep(0.3)
    gitignore_string = "*.pyc\n*.py~\n.idea/*\ndb.sqlite3\ngeckodriver.log"
    call_sp('echo "{}" > ./.gitignore'.format(gitignore_string), **d)
    time.sleep(0.1)
call_sp('git add -A',   **d)
call_sp('git commit -m "initial commit for {}"'.format(project_name), **d)
call_sp('git push -u origin master', **d)
