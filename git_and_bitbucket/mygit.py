#!/usr/bin/env python
# coding: utf-8

"""
to delete:
    mygit delete repo1 repo2 repoN etc...

"""
import os, sys, subprocess, time, re, shutil

from ez_scrip_lib import *

hoss = BitbucketAPIHoss()

args = sys.argv[1:]

if args[0] == 'delete':
    repos_to_delete = args[1:]
    for repo in repos_to_delete:
        hoss.delete_repository(repo)
