#!/usr/bin/env python

import os, sys

from ez_scrip_lib.sysadmin import HOMEPATH, read_lines, read_content, append_content, write_content

LESSONS_PATH = os.path.join(HOMEPATH, "django_class/lessons")
TEMPFILE = os.path.join(LESSONS_PATH, "code_samples/temp")

try:
    TARGET_FILE = sys.argv[1]
except:
    raise Exception("Pass me the target file to send the code to")
    # TARGET_FILE = os.path.join(LESSONS_PATH, "0-setup_your_project.html")


class BashCodeGenerator(object):

    OPEN_DIV = """<div class="{}">\n"""
    CLOSING_DIV = "</div>"

    def __init__(self, css_class="cl"):
        self.source_path = TEMPFILE
        self.target_path = TARGET_FILE
        self.css_class = css_class
        self.add_content_to_lesson()

    def add_content_to_lesson(self):
        print(self.html)
        print(self.target_path)
        append_content(self.target_path, self.html)

    @property
    def html(self):
        return self.OPEN_DIV.format(self.css_class) + self.processed_lines + self.CLOSING_DIV

    @property
    def processed_lines(self):
        string = ""
        for line in self.source_lines:
            string += line + " <br>\n"
        return string

    @property
    def source_lines(self):
        lines = map(lambda line: line.replace('\n', '').replace(' ', '&nbsp;'), read_lines(self.source_path))
        return lines

gen = BashCodeGenerator()
