#!/usr/bin/env python
# coding: utf-8

"""
uninstalls all packages of a certain virtual environments pip (in this case, removing all python2 stuff to use python3 safely)
"""

import os, sys, subprocess, time, re

from ez_scrip_lib import call_sp

args = sys.argv
if len(args) > 1:
    if args[1] == 'python3':
        python3 = True
    else:
        raise Exception('This argument not recognized...')
else:
    python3 = False
    

def get_current_venv_path():
    venv_path = call_sp('echo $VIRTUAL_ENV')
    venv_path = venv_path.replace('\n', '')
    if not python3:
        path = os.path.join(venv_path, 'bin/pip')
    else:
        path = os.path.join(venv_path, 'bin/pip3')
    return path


path = get_current_venv_path()
print('\n\nUninstalling from {}:\n\n'.format(path))


def uninstall_package(package):
    call_sp('{} uninstall {} -y'.format(get_current_venv_path(), package))
    
def get_package_list():
    text = call_sp('{} freeze'.format(get_current_venv_path()))
    as_list = text.split('\n')
    clean_list = [item.split('==', 1)[0] for item in as_list]
    return clean_list
    
packages = get_package_list()

for package in packages:
    print('Uninstalling {}...'.format(package))
    uninstall_package(package)