from django import forms
from django.contrib.auth.models import User


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password',)
        

class EditProfileForm(forms.ModelForm):
    blurb = forms.CharField(required=True)

    class Meta:
        model = User
        fields = ('email',)
