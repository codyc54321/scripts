#!/bin/bash

# https://stackoverflow.com/questions/34303294/how-to-fix-broken-python-2-7-11-after-osx-updates
brew uninstall python
sudo xcode-select --install
brew update && brew reinstall python
