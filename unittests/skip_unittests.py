#!/usr/bin/env python

"""give mode r to run tests, and s to skip:

   $ skiptests filepath r            """

import os, sys, re

filepath    = sys.argv[1]
mode        = sys.argv[2]

def read_lines(the_file):
    with open(the_file, 'r') as f:
        return f.readlines()
        
def write_lines(the_file, lines):
    with open(the_file, 'w') as f:
        f.writelines(lines)

def is_commented(line):
    rgx = r'^\s*[#]'
    return re.match(rgx, line)
    
def comment_out_line(line):
    return '#' + line 
    
def uncomment_line(line):
    return line.replace('#', '', 1)
    
def is_skip_line(line):
    rgx = r'[@]unittest[.]skip'
    return re.search(rgx, line)
    
def process_line(line, mode):
    if is_skip_line(line):
        if mode == 'r':
            if not is_commented(line): return comment_out_line(line)
            else: return line
        elif mode == 's':
            if is_commented(line):     return uncomment_line(line)
            else: return line
    else: return line
        
#----------------------------------------------------------------------------------------------------

if mode.startswith('s'):
    mode = 's'
elif mode.startswith('r'):
    mode = 'r'
else:
    raise 'Please give a mode starting with r or s'


lines = read_lines(filepath)
new_file_lines = []
for line in lines:
    new_file_lines.append(process_line(line, mode))
write_lines(filepath, new_file_lines)
    