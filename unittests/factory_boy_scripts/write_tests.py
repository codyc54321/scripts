#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re, ast

from ez_scriez_scrip_libp_lib import *


MAIN_IMPORT = """\
from django.test import TestCase

from clientsite.{app}.models import (
{models}
)

from model_mommy import mommy


"""


TEST_BLOCK = """\
class {base_model}MommyCreationTests(TestCase):
    
    def setUp(self):
        self.factory_instance = {base_model}Factory()
        self.model = {base_model}

    def test_factory_creates(self):
        self.factory_instance.save()
    
    def test_factory_isinstance(self):
        self.assertTrue(isinstance(self.factory_instance, self.model))


"""

HOMEPATH = os.path.expanduser("~")

PROJECT_PATH = "{}/work_projects/core/unver/clientsite/clientsite".format(HOMEPATH)

MOMMY_PATH = os.path.join(PROJECT_PATH, "model_mommy")

MOMMY_TESTPATH = os.path.join(MOMMY_PATH, "tests")

    

def get_classes_from_file(filepath):
    ast = get_ast_tree_for_file(filepath)
    return parse_items(ast, 'class')
    
def get_classnames(filepath):
    classes = get_classes_from_file(filepath)
    all_names = [klass.name for klass in classes]
    final_names = filter(lambda x: x != "Meta", all_names)
    return final_names
    
def generate_testblock(factory):
    basename = get_base_model(factory)
    return TEST_BLOCK.format(base_model=basename)
    
def generate_test_writepath(factory_path):
    app_name = get_filename_without_extension(factory_path)
    filename = "test_" + app_name + "_factories.py"
    return os.path.join(FACTORY_TESTPATH, filename)

def get_base_names(factory_classes):
    names_list = map(get_base_model, factory_classes)
    return ", ".join(names_list)
    
def get_factory_names(factory_classes):
    return ", ".join(factory_classes)    
    
def generate_models_import_statement(app_name, factory_classes):
    names = get_base_names(factory_classes)
    return "from clientsite.{app_name}.models import {models}\n".format(app_name=app_name, models=names)
    
def generate_factories_import_statement(app_name, factory_classes):
    names = get_factory_names(factory_classes)
    return "from clientsite.factories.{app_name} import {models}\n\n\n".format(app_name=app_name, models=names)
    
def write_tests_to_file(factory_filepath):
    app_name = get_filename_without_extension(factory_filepath)
    factory_classnames = get_classnames(factory_filepath)
    # base_models = get_base_names(factory_classnames)
    writepath = generate_test_writepath(factory_filepath)
    write_content(writepath, "from django.test import TestCase\n\n")
    append_content(writepath, generate_models_import_statement(app_name, factory_classnames))
    append_content(writepath, generate_factories_import_statement(app_name, factory_classnames))
    for factory in factory_classnames:
        testblock = generate_testblock(factory)
        append_content(writepath, testblock)
        
    
EXAMPLE = '~/work_projects/core/unver/clientsite/clientsite/factories/relate.py'

assert(generate_test_writepath(EXAMPLE) == '~/work_projects/core/unver/clientsite/clientsite/factories/tests/test_relate_factories.py')


files = get_filenames(FACTORY_PATH, banned_files=('.pyc', '__init__.py'))

# for f in files:
#     write_tests_to_file(f)


write_tests_to_file(EXAMPLE)
